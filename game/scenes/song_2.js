class Song2 extends Phaser.Scene {
    constructor() {
        super({ key: "Song2" });

        var sceneStartTime;
        var sceneCurrentTime;

        var songStartTime;
        var songCurrentTime;
        var songCurrentProgress;

        var song;

        var textStartCountdown;

        var spriteHighlightStrip0;
        var spriteHighlightStrip1;
        var spriteHighlightStrip2;
        var spriteHighlightStrip3;
        var spriteHighlightStrip4;
        var spriteHighlightStrip5;

        // input handler
        var key0;
        var key1;
        var key2;
        var key3;
        var key4;
        var key5;
        // input handler end

        var blockTimestamps;
        var activeBlocks; // current active blocks

        var noteTrigger0;
        var noteTrigger1;
        var noteTrigger2;
        var noteTrigger3;
        var noteTrigger4;
        var noteTrigger5;

        // score and combo
        var score;
        var textScore;
        var textStatus;
        var combo;
        var countDownVisibleTimer;

        var speed;
    }

    preload() {
        this.load.image("background_gameplay", "/game/assets/backgrounds/gameplay.png");
        this.load.audio("song", ["/game/assets/jake_kaufman/crypt_of_the_necrodancer_-_freestyle_retro/02_disco_descent_(1-1_remix).ogg"]);

        this.load.image("kp_highlight_strip", "/game/assets/sprites/keypress_highlight_strip.png");

        this.load.image("note_block_pink", "/game/assets/sprites/note_block_pink.png");
        this.load.image("note_block_blue", "/game/assets/sprites/note_block_blue.png");

        this.load.image("note_trigger", "/game/assets/triggers/note_trigger.png");
    }

    create() {
        var background = this.add.image(0, 0, "background_gameplay");
        background.setOrigin(0, 0);

        this.input.keyboard.on(
            "keydown_ESC",
            function () {
                // location.href = "/game/index.html";
                this.song.stop();
                location.href = "/game/index.html";
                // this.scene.start("MainMenu");
            },
            this
        );

        this.sceneStartTime = 0;
        this.sceneCurrentTime = 0;

        this.songStartTime = 0;
        this.songCurrentTime = 0;
        this.songCurrentProgress = 0;

        this.song = this.sound.add("song");

        this.textStartCountdown = this.add.text(158, 40, "5", { font: "24px PressStart2P-Regular", align: "center" });
        this.textStartCountdown.setOrigin(0.5, 0.5);

        this.spriteHighlightStrip0 = this.add.image(68, 191, "kp_highlight_strip");
        this.spriteHighlightStrip0.setOrigin(0, 1);
        this.spriteHighlightStrip1 = this.add.image(98, 191, "kp_highlight_strip");
        this.spriteHighlightStrip1.setOrigin(0, 1);
        this.spriteHighlightStrip2 = this.add.image(128, 191, "kp_highlight_strip");
        this.spriteHighlightStrip2.setOrigin(0, 1);
        this.spriteHighlightStrip3 = this.add.image(158, 191, "kp_highlight_strip");
        this.spriteHighlightStrip3.setOrigin(0, 1);
        this.spriteHighlightStrip4 = this.add.image(188, 191, "kp_highlight_strip");
        this.spriteHighlightStrip4.setOrigin(0, 1);
        this.spriteHighlightStrip5 = this.add.image(218, 191, "kp_highlight_strip");
        this.spriteHighlightStrip5.setOrigin(0, 1);

        this.key0 = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.LEFT);
        this.key1 = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.UP);
        this.key2 = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.RIGHT);
        this.key3 = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.U);
        this.key4 = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.I);
        this.key5 = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.K);

        this.noteTrigger0 = this.physics.add.image(68, 198, "note_trigger");
        this.noteTrigger0.setOrigin(0, 1);
        this.noteTrigger0.setImmovable(true);
        this.noteTrigger1 = this.physics.add.image(98, 198, "note_trigger");
        this.noteTrigger1.setOrigin(0, 1);
        this.noteTrigger1.setImmovable(true);
        this.noteTrigger2 = this.physics.add.image(128, 198, "note_trigger");
        this.noteTrigger2.setOrigin(0, 1);
        this.noteTrigger2.setImmovable(true);
        this.noteTrigger3 = this.physics.add.image(158, 198, "note_trigger");
        this.noteTrigger3.setOrigin(0, 1);
        this.noteTrigger3.setImmovable(true);
        this.noteTrigger4 = this.physics.add.image(188, 198, "note_trigger");
        this.noteTrigger4.setOrigin(0, 1);
        this.noteTrigger4.setImmovable(true);
        this.noteTrigger5 = this.physics.add.image(218, 198, "note_trigger");
        this.noteTrigger5.setOrigin(0, 1);
        this.noteTrigger5.setImmovable(true);

        this.score = 0;
        this.textScore = this.add.text(158, 40, "0", { font: "16px PressStart2P-Regular", align: "center" });
        this.textScore.setOrigin(0.5, 0.5);
        this.textScore.visible = false;
        this.textStatus = this.add.text(158, 58, "PERFECT", { font: "12px PressStart2P-Regular", align: "center" });
        this.textStatus.setOrigin(0.5, 0.5);
        this.textStatus.visible = false;
        this.combo = 0;
        this.countDownVisibleTimer = 1;

        this.speed = 1.5;

        this.input.keyboard.on(
            "keydown_SPACE",
            function () {
                // slow down speed
                if (this.speed == 1.5) {
                    this.speed = 1;
                } else if (this.speed == 2) {
                    this.speed = 1.5;
                } else if (this.speed == 2.5) {
                    this.speed = 2;
                }
                // console.log(this.speed);
            },
            this
        );

        this.input.keyboard.on(
            "keydown_ENTER",
            function () {
                // increase speed
                if (this.speed == 1) {
                    this.speed = 1.5;
                } else if (this.speed == 1.5) {
                    this.speed = 2;
                } else if (this.speed == 2) {
                    this.speed = 2.5;
                }
                // console.log(this.speed);
            },
            this
        );

        // block spawn lanes and times
        // TODO: add notes to current song
        this.blockTimestamps = [
            { lane: 3, time: 4.259360000000015 },
            { lane: 3, time: 4.259360000000015 },
            { lane: 2, time: 4.259360000000015 },
            { lane: 1, time: 6.278010000001814 },
            { lane: 1, time: 6.278010000001814 },
            { lane: 4, time: 6.278010000001814 },
            { lane: 2, time: 7.062130000002071 },
            { lane: 3, time: 7.0788550000033865 },
            { lane: 2, time: 8.41342000000077 },
            { lane: 2, time: 8.930790000002162 },
            { lane: 2, time: 9.447800000001735 },
            { lane: 2, time: 9.948310000003403 },
            { lane: 3, time: 10.482145000001765 },
            { lane: 3, time: 11.032710000003135 },
            { lane: 3, time: 11.549980000003416 },
            { lane: 3, time: 12.050365000002785 },
            { lane: 4, time: 12.567595000000438 },
            { lane: 4, time: 13.118100000003324 },
            { lane: 4, time: 13.651985000000423 },
            { lane: 4, time: 14.15249500000209 },
            { lane: 5, time: 14.653150000001915 },
            { lane: 5, time: 15.170155000003433 },
            { lane: 0, time: 15.72070000000167 },
            { lane: 0, time: 16.237920000003214 },
            { lane: 1, time: 16.77176000000327 },
            { lane: 1, time: 17.288900000003196 },
            { lane: 3, time: 17.756015000002662 },
            { lane: 3, time: 18.2899000000034 },
            { lane: 2, time: 18.823765000001004 },
            { lane: 2, time: 19.374305000001186 },
            { lane: 3, time: 19.85812000000078 },
            { lane: 3, time: 20.391985000002023 },
            { lane: 0, time: 20.892550000000483 },
            { lane: 4, time: 21.443065000003116 },
            { lane: 1, time: 21.94360500000039 },
            { lane: 5, time: 22.44405000000188 },
            { lane: 2, time: 23.01131500000338 },
            { lane: 2, time: 23.01131500000338 },
            { lane: 3, time: 23.01131500000338 },
            { lane: 2, time: 23.54519000000073 },
            { lane: 2, time: 23.54519000000073 },
            { lane: 3, time: 23.54519000000073 },
            { lane: 4, time: 24.02896000000328 },
            { lane: 4, time: 24.02896000000328 },
            { lane: 1, time: 24.02896000000328 },
            { lane: 5, time: 24.5295100000003 },
            { lane: 5, time: 24.5295100000003 },
            { lane: 0, time: 24.5295100000003 },
            { lane: 2, time: 25.080025000002934 },
            { lane: 2, time: 25.080025000002934 },
            { lane: 3, time: 25.080025000002934 },
            { lane: 3, time: 29.250825000002806 },
            { lane: 3, time: 30.30182500000228 },
            { lane: 2, time: 31.352960000000166 },
            { lane: 2, time: 31.886725000000297 },
            { lane: 2, time: 32.420610000001034 },
            { lane: 2, time: 32.92110500000126 },
            { lane: 4, time: 33.421620000000985 },
            { lane: 3, time: 35.24010500000077 },
            { lane: 4, time: 35.49033000000054 },
            { lane: 3, time: 36.27494000000297 },
            { lane: 4, time: 36.524730000001 },
            { lane: 3, time: 37.00859000000128 },
            { lane: 2, time: 37.60915500000192 },
            { lane: 2, time: 38.12637500000346 },
            { lane: 2, time: 38.626915000000736 },
            { lane: 2, time: 39.17733500000031 },
            { lane: 4, time: 39.69453000000067 },
            { lane: 2, time: 39.71113500000138 },
            { lane: 2, time: 40.2451850000034 },
            { lane: 3, time: 40.72885000000315 },
            { lane: 2, time: 40.74557500000083 },
            { lane: 4, time: 41.24606000000131 },
            { lane: 4, time: 41.24606000000131 },
            { lane: 2, time: 41.24606000000131 },
            { lane: 5, time: 41.77988000000187 },
            { lane: 2, time: 41.81330500000331 },
            { lane: 2, time: 42.29708000000028 },
            { lane: 2, time: 42.814275000000634 },
            { lane: 2, time: 43.3814850000017 },
            { lane: 1, time: 43.88207500000135 },
            { lane: 1, time: 44.3824900000036 },
            { lane: 3, time: 44.91640500000358 },
            { lane: 4, time: 45.43353500000012 },
            { lane: 1, time: 45.96742000000086 },
            { lane: 5, time: 46.48460000000341 },
            { lane: 0, time: 47.0351350000019 },
            { lane: 0, time: 47.53564500000357 },
            { lane: 5, time: 48.0527800000018 },
            { lane: 5, time: 48.5699900000036 },
            { lane: 4, time: 49.0872150000032 },
            { lane: 4, time: 49.60434500000338 },
            { lane: 3, time: 50.121540000000095 },
            { lane: 2, time: 50.65536500000235 },
            { lane: 3, time: 51.18924000000334 },
            { lane: 4, time: 51.68978000000061 },
            { lane: 2, time: 52.240350000000035 },
            { lane: 1, time: 52.75747000000047 },
            { lane: 2, time: 53.224685000001045 },
            { lane: 3, time: 53.75849500000186 },
            { lane: 0, time: 54.30903500000204 },
            { lane: 0, time: 54.842890000003536 },
            { lane: 1, time: 55.34338000000207 },
            { lane: 2, time: 55.877510000002076 },
            { lane: 3, time: 56.361150000000634 },
            { lane: 4, time: 56.92833000000246 },
            { lane: 5, time: 57.4454450000012 },
            { lane: 4, time: 57.979355000003125 },
            { lane: 3, time: 58.196295000001555 },
            { lane: 2, time: 58.46313000000009 },
            { lane: 2, time: 58.996995000001334 },
            { lane: 2, time: 59.53086500000063 },
            { lane: 2, time: 60.048085000002175 },
            { lane: 3, time: 60.56525500000134 },
            { lane: 3, time: 61.11580000000322 },
            { lane: 4, time: 61.63304500000231 },
            { lane: 4, time: 61.849850000002334 },
            { lane: 4, time: 62.10022000000026 },
            { lane: 4, time: 62.383850000001985 },
            { lane: 3, time: 62.65068500000052 },
            { lane: 3, time: 62.65068500000052 },
            { lane: 2, time: 62.65068500000052 },
            { lane: 2, time: 63.184540000002016 },
            { lane: 1, time: 63.70168000000194 },
            { lane: 2, time: 64.21886000000086 },
            { lane: 5, time: 64.7528850000017 },
            { lane: 0, time: 65.28659000000334 },
            { lane: 5, time: 65.8038000000015 },
            { lane: 0, time: 66.33764500000325 },
            { lane: 1, time: 66.83908000000156 },
            { lane: 1, time: 66.83908000000156 },
            { lane: 4, time: 66.83908000000156 },
            { lane: 2, time: 67.35538000000088 },
            { lane: 1, time: 67.87249500000325 },
            { lane: 2, time: 68.37301500000103 },
            { lane: 3, time: 68.90687000000253 },
            { lane: 4, time: 69.40741000000344 },
            { lane: 5, time: 69.9412650000013 },
            { lane: 4, time: 70.47524500000145 },
            { lane: 3, time: 70.99229000000196 },
            { lane: 2, time: 71.24252500000148 },
            { lane: 1, time: 71.49278500000219 },
            { lane: 2, time: 72.04334500000186 },
            { lane: 3, time: 72.56049500000154 },
            { lane: 0, time: 73.04452500000116 },
            { lane: 1, time: 73.32795500000066 },
            { lane: 2, time: 73.56156500000361 },
            { lane: 3, time: 73.82844500000283 },
            { lane: 4, time: 74.09544000000096 },
            { lane: 5, time: 74.62923000000228 },
            { lane: 4, time: 75.14646500000163 },
            { lane: 3, time: 75.41343500000221 },
            { lane: 2, time: 75.64696000000185 },
            { lane: 1, time: 75.91389000000345 },
            { lane: 2, time: 76.18081500000335 },
            { lane: 3, time: 76.4477850000003 },
            { lane: 4, time: 76.73130500000116 },
            { lane: 3, time: 77.2151150000027 },
            { lane: 4, time: 77.44871000000057 },
            { lane: 5, time: 77.71561500000098 },
            { lane: 4, time: 78.03262500000346 },
            { lane: 3, time: 78.28289000000223 },
            { lane: 2, time: 78.54980500000238 },
            { lane: 1, time: 78.81672000000253 },
            { lane: 2, time: 79.33398500000112 },
            { lane: 2, time: 79.8677450000032 },
            { lane: 3, time: 80.4016300000003 },
            { lane: 4, time: 80.93549000000348 },
            { lane: 0, time: 81.45270000000164 },
            { lane: 1, time: 81.98655000000144 },
            { lane: 2, time: 82.47038500000053 },
            { lane: 3, time: 82.97083000000202 },
            { lane: 4, time: 83.53812000000107 },
            { lane: 4, time: 84.57246000000305 },
            { lane: 3, time: 85.62347000000227 },
            { lane: 4, time: 86.10724500000288 },
            { lane: 5, time: 86.64113000000361 },
            { lane: 4, time: 87.15833000000202 },
            { lane: 3, time: 87.65881000000081 },
            { lane: 2, time: 88.19274500000029 },
            { lane: 2, time: 88.75991500000237 },
            { lane: 2, time: 89.27710000000297 },
            { lane: 1, time: 89.8110050000032 },
            { lane: 1, time: 90.32813500000339 },
            { lane: 4, time: 90.8120450000024 },
            { lane: 4, time: 90.8120450000024 },
            { lane: 1, time: 90.8120450000024 },
            { lane: 1, time: 91.36253000000215 },
            { lane: 1, time: 91.8467550000023 },
            { lane: 3, time: 91.862975 },
            { lane: 2, time: 92.38023000000248 },
            { lane: 2, time: 92.91412000000128 },
            { lane: 2, time: 93.44793500000014 },
            { lane: 4, time: 93.931700000001 },
            { lane: 2, time: 93.96514000000025 },
            { lane: 5, time: 94.48225000000093 },
            { lane: 5, time: 94.96610000000146 },
            { lane: 5, time: 95.50003000000288 },
            { lane: 2, time: 96.033835000002 },
            { lane: 2, time: 96.033835000002 },
            { lane: 3, time: 96.033835000002 },
            { lane: 1, time: 96.53431000000273 },
            { lane: 0, time: 97.06820500000322 },
            { lane: 1, time: 97.58535500000289 },
            { lane: 2, time: 98.10259500000029 },
            { lane: 4, time: 98.63639500000136 },
            { lane: 5, time: 98.83659000000262 },
            { lane: 4, time: 99.13691500000277 },
            { lane: 3, time: 99.6874850000022 },
            { lane: 4, time: 100.18800000000192 },
            { lane: 3, time: 100.73850000000311 },
            { lane: 4, time: 101.25568500000008 },
            { lane: 5, time: 101.78953500000353 },
            { lane: 2, time: 102.27337500000067 },
            { lane: 2, time: 102.27337500000067 },
            { lane: 3, time: 102.27337500000067 },
            { lane: 1, time: 102.80718000000343 },
            { lane: 2, time: 103.32436000000234 },
            { lane: 4, time: 103.8916100000024 },
            { lane: 3, time: 104.42547000000195 },
            { lane: 5, time: 104.90933500000028 },
            { lane: 4, time: 105.44313500000135 },
            { lane: 3, time: 105.97705000000133 },
            { lane: 4, time: 106.494200000001 },
            { lane: 5, time: 106.9947650000031 },
            { lane: 2, time: 107.5118500000026 },
            { lane: 2, time: 107.5118500000026 },
            { lane: 3, time: 107.5118500000026 },
            { lane: 2, time: 108.04579000000012 },
            { lane: 2, time: 108.04579000000012 },
            { lane: 3, time: 108.04579000000012 },
            { lane: 1, time: 108.56291500000225 },
            { lane: 1, time: 109.08010000000286 },
            { lane: 1, time: 109.58060500000283 },
            { lane: 1, time: 110.08110000000306 },
            { lane: 2, time: 110.648290000001 },
            { lane: 2, time: 111.1654950000011 },
            { lane: 2, time: 111.66607000000295 },
            { lane: 2, time: 112.19986000000063 },
            { lane: 3, time: 112.71705500000098 },
            { lane: 3, time: 113.23424000000159 },
            { lane: 3, time: 113.76813000000038 },
            { lane: 3, time: 114.30201500000112 },
            { lane: 4, time: 114.80247000000236 },
            { lane: 4, time: 115.31960500000059 },
            { lane: 4, time: 115.88693500000227 },
            { lane: 4, time: 116.35399000000325 },
            { lane: 5, time: 116.87115000000267 },
            { lane: 5, time: 117.43842500000028 },
            { lane: 5, time: 117.93889500000296 },
            { lane: 5, time: 118.42271500000061 },
            { lane: 4, time: 118.70636000000013 },
            { lane: 3, time: 118.99004500000228 },
            { lane: 4, time: 119.50714500000322 },
            { lane: 3, time: 120.0576850000034 },
            { lane: 4, time: 120.25784500000009 },
            { lane: 3, time: 120.55825000000186 },
            { lane: 2, time: 121.09206000000268 },
            { lane: 1, time: 121.6258600000001 },
            { lane: 2, time: 122.12643500000195 },
            { lane: 1, time: 122.6436450000001 },
            { lane: 2, time: 123.16075500000079 },
            { lane: 3, time: 123.3943700000018 },
            { lane: 4, time: 123.66128000000026 },
            { lane: 5, time: 124.24515000000247 },
            { lane: 4, time: 124.7456600000005 },
            { lane: 0, time: 125.27955000000293 },
            { lane: 0, time: 125.79670500000066 },
            { lane: 0, time: 126.29722500000207 },
            { lane: 0, time: 126.78104500000336 },
            { lane: 5, time: 127.36493000000337 },
            { lane: 5, time: 127.89881500000047 },
            { lane: 1, time: 128.3993100000007 },
            { lane: 1, time: 128.93320000000313 },
            { lane: 4, time: 129.45039500000348 },
            { lane: 4, time: 129.95079500000065 },
            { lane: 2, time: 130.5180450000007 },
            { lane: 3, time: 131.03523000000132 },
            { lane: 2, time: 131.53572500000155 },
            { lane: 3, time: 132.0695900000028 },
            { lane: 4, time: 132.57013000000006 },
            { lane: 5, time: 133.07067500000267 },
            { lane: 1, time: 133.62116500000047 },
            { lane: 1, time: 134.12163500000315 },
            { lane: 1, time: 134.67219500000283 },
            { lane: 1, time: 135.18938000000344 },
            { lane: 2, time: 135.67317000000185 },
            { lane: 4, time: 135.92348500000298 },
            { lane: 5, time: 136.1570550000033 },
            { lane: 5, time: 136.74091500000213 },
            { lane: 5, time: 137.24138500000117 },
            { lane: 4, time: 137.77525500000047 },
            { lane: 5, time: 138.30910000000222 },
            { lane: 4, time: 138.842990000001 },
            { lane: 5, time: 139.3268250000001 },
            { lane: 4, time: 139.89416500000152 },
            { lane: 3, time: 140.41120500000034 },
            { lane: 2, time: 140.9117600000027 },
            { lane: 1, time: 141.4121750000013 },
            { lane: 2, time: 141.92945000000327 },
            { lane: 3, time: 142.47994500000277 },
            { lane: 4, time: 143.01380000000063 },
            { lane: 5, time: 143.5143300000018 },
            { lane: 4, time: 144.01482500000202 },
            { lane: 5, time: 144.56537000000026 },
            { lane: 4, time: 145.08254000000306 },
            { lane: 3, time: 145.59974500000317 },
            { lane: 2, time: 146.10018000000127 },
            { lane: 3, time: 146.6507500000007 },
            { lane: 2, time: 147.16788500000257 },
            { lane: 3, time: 147.6851850000021 },
            { lane: 4, time: 148.16900500000338 },
            { lane: 5, time: 148.73621000000276 },
            { lane: 4, time: 149.2366600000023 },
            { lane: 3, time: 149.7704900000026 },
            { lane: 1, time: 150.28828000000067 },
            { lane: 2, time: 151.33873000000312 },
            { lane: 4, time: 151.8559200000018 },
            { lane: 2, time: 152.37311000000045 },
            { lane: 3, time: 152.90700500000094 },
            { lane: 2, time: 153.44088000000193 },
            { lane: 3, time: 153.92463000000134 },
            { lane: 1, time: 154.45848000000115 },
            { lane: 1, time: 155.509530000003 },
            { lane: 4, time: 156.04340500000035 },
            { lane: 2, time: 156.54387500000303 },
            { lane: 3, time: 157.12778500000059 },
            { lane: 5, time: 157.61167000000205 },
            { lane: 0, time: 158.1454900000026 },
            { lane: 4, time: 158.6459750000031 },
            { lane: 4, time: 158.6459750000031 },
            { lane: 1, time: 158.6459750000031 },
            { lane: 5, time: 159.14650000000256 },
            { lane: 0, time: 159.69697500000257 },
            { lane: 5, time: 160.21419000000242 },
            { lane: 1, time: 160.69806000000244 },
            { lane: 4, time: 160.71474000000308 },
            { lane: 5, time: 161.26523000000088 },
            { lane: 0, time: 161.28190500000346 },
            { lane: 1, time: 161.79909000000043 },
            { lane: 2, time: 162.33309500000178 },
            { lane: 3, time: 162.81678000000102 },
            { lane: 4, time: 163.3507150000005 },
            { lane: 5, time: 163.86791000000085 },
            { lane: 4, time: 164.4017300000014 },
            { lane: 3, time: 164.88550000000032 },
            { lane: 2, time: 165.41941000000224 },
            { lane: 4, time: 165.95329500000298 },
            { lane: 5, time: 166.45373500000278 },
            { lane: 1, time: 167.00428000000102 },
            { lane: 1, time: 167.571565000002 },
            { lane: 1, time: 168.05533500000092 },
            { lane: 1, time: 168.53922500000044 },
            { lane: 0, time: 169.05635000000257 },
            { lane: 1, time: 169.59019000000262 },
            { lane: 1, time: 170.12405000000217 },
            { lane: 1, time: 170.64126000000033 },
            { lane: 2, time: 171.14174000000276 },
            { lane: 3, time: 171.69237500000236 },
            { lane: 2, time: 172.24284500000067 },
            { lane: 3, time: 172.76005500000247 },
            { lane: 1, time: 173.22712500000125 },
            { lane: 4, time: 173.77771000000212 },
            { lane: 5, time: 174.29492500000197 },
            { lane: 4, time: 174.84537000000273 },
            { lane: 3, time: 175.36259500000233 },
        ];
        this.activeBlocks = [];
    }

    getBlockSpawnYVelocity(speed = 1) {
        return speed * 96; // default speed is 96
    }

    getBlockSpawnYPosition(speed = 1) {
        return -((speed - 1) * 192); // default travel distance is 192
    }

    spawnBlock(lane, speed) {
        var that = this;
        if (lane >= 0 && lane <= 5) {
            var newBlock;
            switch (lane) {
                case 0:
                    newBlock = this.physics.add.image(68 + 14.5, this.getBlockSpawnYPosition(speed), "note_block_pink");
                    this.physics.add.overlap(newBlock, this.noteTrigger0, function () {
                        if (that.key0.isDown && newBlock.visible) {
                            newBlock.visible = false;
                            if (newBlock.y < 178) {
                                // console.log("missed"); // missed by early press
                                that.textStatus.setText("MISSED");
                                that.textStatus.visible = true;
                                that.countDownVisibleTimer = 1;
                                that.combo = 0;
                            } else {
                                if (newBlock.y >= 185 && newBlock.y <= 202) {
                                    // console.log("perfect"); // perfect hit
                                    that.score = that.score + (10 + 5 * (that.combo / 10));
                                    that.combo = that.combo + 1;

                                    that.textStatus.setText("PERFECT");
                                    that.textStatus.visible = true;
                                    that.countDownVisibleTimer = 1;
                                } else {
                                    // console.log("good"); // good hit
                                    that.score = that.score + (8 + 5 * (that.combo / 10));
                                    that.combo = that.combo + 1;

                                    that.textStatus.setText("GOOD");
                                    that.textStatus.visible = true;
                                    that.countDownVisibleTimer = 1;
                                }
                            }
                        }
                    });
                    break;
                case 1:
                    newBlock = this.physics.add.image(98 + 14.5, this.getBlockSpawnYPosition(speed), "note_block_blue");
                    this.physics.add.overlap(newBlock, this.noteTrigger1, function () {
                        if (that.key1.isDown && newBlock.visible) {
                            newBlock.visible = false;
                            if (newBlock.y < 178) {
                                that.textStatus.setText("MISSED");
                                that.textStatus.visible = true;
                                that.countDownVisibleTimer = 1;
                                that.combo = 0;
                            } else {
                                if (newBlock.y >= 185 && newBlock.y <= 202) {
                                    that.score = that.score + (10 + 5 * (that.combo / 10));
                                    that.combo = that.combo + 1;

                                    that.textStatus.setText("PERFECT");
                                    that.textStatus.visible = true;
                                    that.countDownVisibleTimer = 1;
                                } else {
                                    that.score = that.score + (8 + 5 * (that.combo / 10));
                                    that.combo = that.combo + 1;

                                    that.textStatus.setText("GOOD");
                                    that.textStatus.visible = true;
                                    that.countDownVisibleTimer = 1;
                                }
                            }
                        }
                    });
                    break;
                case 2:
                    newBlock = this.physics.add.image(128 + 14.5, this.getBlockSpawnYPosition(speed), "note_block_pink");
                    this.physics.add.overlap(newBlock, this.noteTrigger2, function () {
                        if (that.key2.isDown && newBlock.visible) {
                            newBlock.visible = false;
                            if (newBlock.y < 178) {
                                that.textStatus.setText("MISSED");
                                that.textStatus.visible = true;
                                that.countDownVisibleTimer = 1;
                                that.combo = 0;
                            } else {
                                if (newBlock.y >= 185 && newBlock.y <= 202) {
                                    that.score = that.score + (10 + 5 * (that.combo / 10));
                                    that.combo = that.combo + 1;

                                    that.textStatus.setText("PERFECT");
                                    that.textStatus.visible = true;
                                    that.countDownVisibleTimer = 1;
                                } else {
                                    that.score = that.score + (8 + 5 * (that.combo / 10));
                                    that.combo = that.combo + 1;

                                    that.textStatus.setText("GOOD");
                                    that.textStatus.visible = true;
                                    that.countDownVisibleTimer = 1;
                                }
                            }
                        }
                    });
                    break;
                case 3:
                    newBlock = this.physics.add.image(158 + 14.5, this.getBlockSpawnYPosition(speed), "note_block_pink");
                    this.physics.add.overlap(newBlock, this.noteTrigger3, function () {
                        if (that.key3.isDown && newBlock.visible) {
                            newBlock.visible = false;
                            if (newBlock.y < 178) {
                                that.textStatus.setText("MISSED");
                                that.textStatus.visible = true;
                                that.countDownVisibleTimer = 1;
                                that.combo = 0;
                            } else {
                                if (newBlock.y >= 185 && newBlock.y <= 202) {
                                    that.score = that.score + (10 + 5 * (that.combo / 10));
                                    that.combo = that.combo + 1;

                                    that.textStatus.setText("PERFECT");
                                    that.textStatus.visible = true;
                                    that.countDownVisibleTimer = 1;
                                } else {
                                    that.score = that.score + (8 + 5 * (that.combo / 10));
                                    that.combo = that.combo + 1;

                                    that.textStatus.setText("GOOD");
                                    that.textStatus.visible = true;
                                    that.countDownVisibleTimer = 1;
                                }
                            }
                        }
                    });
                    break;
                case 4:
                    newBlock = this.physics.add.image(188 + 14.5, this.getBlockSpawnYPosition(speed), "note_block_blue");
                    this.physics.add.overlap(newBlock, this.noteTrigger4, function () {
                        if (that.key4.isDown && newBlock.visible) {
                            newBlock.visible = false;
                            if (newBlock.y < 178) {
                                that.textStatus.setText("MISSED");
                                that.textStatus.visible = true;
                                that.countDownVisibleTimer = 1;
                                that.combo = 0;
                            } else {
                                if (newBlock.y >= 185 && newBlock.y <= 202) {
                                    that.score = that.score + (10 + 5 * (that.combo / 10));
                                    that.combo = that.combo + 1;

                                    that.textStatus.setText("PERFECT");
                                    that.textStatus.visible = true;
                                    that.countDownVisibleTimer = 1;
                                } else {
                                    that.score = that.score + (8 + 5 * (that.combo / 10));
                                    that.combo = that.combo + 1;

                                    that.textStatus.setText("GOOD");
                                    that.textStatus.visible = true;
                                    that.countDownVisibleTimer = 1;
                                }
                            }
                        }
                    });
                    break;
                case 5:
                    newBlock = this.physics.add.image(218 + 14.5, this.getBlockSpawnYPosition(speed), "note_block_pink");
                    this.physics.add.overlap(newBlock, this.noteTrigger5, function () {
                        if (that.key5.isDown && newBlock.visible) {
                            newBlock.visible = false;
                            if (newBlock.y < 178) {
                                that.textStatus.setText("MISSED");
                                that.textStatus.visible = true;
                                that.countDownVisibleTimer = 1;
                                that.combo = 0;
                            } else {
                                if (newBlock.y >= 185 && newBlock.y <= 202) {
                                    that.score = that.score + (10 + 5 * (that.combo / 10));
                                    that.combo = that.combo + 1;

                                    that.textStatus.setText("PERFECT");
                                    that.textStatus.visible = true;
                                    that.countDownVisibleTimer = 1;
                                } else {
                                    that.score = that.score + (8 + 5 * (that.combo / 10));
                                    that.combo = that.combo + 1;

                                    that.textStatus.setText("GOOD");
                                    that.textStatus.visible = true;
                                    that.countDownVisibleTimer = 1;
                                }
                            }
                        }
                    });
                    break;
            }
            newBlock.setOrigin(0, 0.5);
            newBlock.setVelocityY(this.getBlockSpawnYVelocity(speed));

            this.activeBlocks.push(newBlock);
        }
    }

    update(time, delta) {
        if (this.sceneStartTime == 0) {
            this.sceneStartTime = time;
        }
        this.sceneCurrentTime = time - this.sceneStartTime;
        if (this.sceneCurrentTime < 5000) {
            this.textStartCountdown.setText(5 - (this.sceneCurrentTime / 1000).toFixed(0));
        } else if (this.sceneCurrentTime >= 5000 && !this.song.isPlaying && this.songCurrentProgress == 0) {
            // song starts here
            this.textStartCountdown.visible = false;
            this.song.play();
            this.songStartTime = this.sceneCurrentTime;
            this.textScore.visible = true;
        } else if (this.song.isPlaying) {
            // song playing here
            this.songCurrentTime = (this.sceneCurrentTime - this.songStartTime) / 1000; // in second
            var remainingTime = this.song.duration + this.sceneCurrentTime / 1000 - (this.songCurrentTime + this.sceneCurrentTime / 1000); // in second
            this.songCurrentProgress = 1 - remainingTime / this.song.duration; // 0 - 1

            // spawn blocks here
            this.blockTimestamps.forEach((block) => {
                if (this.songCurrentTime >= block.time - 2.1) {
                    this.spawnBlock(block.lane, this.speed); // set spawn speed here
                    block.lane = -1;
                    block.time = this.song.duration * 255;
                }
            });

            // remove active blocks here
            for (var i = 0; i < this.activeBlocks.length; i++) {
                if (this.activeBlocks[i].y >= 203) {
                    if (this.activeBlocks[i].visible) {
                        // console.log("missed"); // missed by late press
                        this.combo = 0;

                        this.textStatus.setText("MISSED");
                        this.textStatus.visible = true;
                        this.countDownVisibleTimer = 1;
                    }
                    this.activeBlocks[i].destroy();
                    this.activeBlocks.splice(i, 1);
                }
            }

            this.textScore.setText(this.score.toFixed(0));

            if (this.countDownVisibleTimer > 0) {
                this.countDownVisibleTimer = this.countDownVisibleTimer - delta / 1000;
            } else {
                this.textStatus.visible = false;
            }
        } else {
            // song end
            this.textStatus.setText("Press MENU to Return");
            this.textStatus.y = 90;
            this.textStatus.visible = true;
        }

        if (this.key0.isDown) {
            this.spriteHighlightStrip0.visible = true;
        } else {
            this.spriteHighlightStrip0.visible = false;
        }

        if (this.key1.isDown) {
            this.spriteHighlightStrip1.visible = true;
        } else {
            this.spriteHighlightStrip1.visible = false;
        }

        if (this.key2.isDown) {
            this.spriteHighlightStrip2.visible = true;
        } else {
            this.spriteHighlightStrip2.visible = false;
        }

        if (this.key3.isDown) {
            this.spriteHighlightStrip3.visible = true;
        } else {
            this.spriteHighlightStrip3.visible = false;
        }

        if (this.key4.isDown) {
            this.spriteHighlightStrip4.visible = true;
        } else {
            this.spriteHighlightStrip4.visible = false;
        }

        if (this.key5.isDown) {
            this.spriteHighlightStrip5.visible = true;
        } else {
            this.spriteHighlightStrip5.visible = false;
        }
    }
}
