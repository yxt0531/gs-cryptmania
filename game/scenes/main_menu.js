class MainMenu extends Phaser.Scene {
    constructor() {
        super({ key: "MainMenu" });

        var textCurrentSong;
        var textCurrentSongSubtitle;

        var songs;
        var currentSongIndex;

        var textStart;
        var textExit;

        var currentAction;
    }

    preload() {
        this.load.image("background", "/game/assets/backgrounds/main_menu.png");

        this.load.audio("song_1", ["/game/assets/jake_kaufman/crypt_of_the_necrodancer_-_freestyle_retro/01_rhythmortis_(lobby_remix)_prev.ogg"]);
        this.load.audio("song_2", ["/game/assets/jake_kaufman/crypt_of_the_necrodancer_-_freestyle_retro/02_disco_descent_(1-1_remix)_prev.ogg"]);
        this.load.audio("song_3", ["/game/assets/jake_kaufman/crypt_of_the_necrodancer_-_freestyle_retro/03_crypteque_(1-2_remix)_prev.ogg"]);
        this.load.audio("song_4", ["/game/assets/jake_kaufman/crypt_of_the_necrodancer_-_freestyle_retro/04_mauseoleum_mash_(1-3_remix)_prev.ogg"]);
        this.load.audio("song_5", ["/game/assets/jake_kaufman/crypt_of_the_necrodancer_-_freestyle_retro/05_konga_conga_kappa_(king_konga_remix)_prev.ogg"]);
    }

    create() {
        // save start count
        var that = this;

        var saved_song_index = {
            index: 0,
        };
        const fs = require("fs");
        fs.readFile("./saves/main_menu.json", function (err, data) {
            if (err) {
                // create save file if not exist
                let outfile = fs.createWriteStream("./saves/main_menu.json");
                outfile.write(JSON.stringify(saved_song_index));
                outfile.end();

                that.currentSongIndex = 0;
                that.playSong(that.currentSongIndex);
            } else {
                // load from save file
                saved_song_index = JSON.parse(data.toString());
                that.currentSongIndex = saved_song_index.index;
                that.playSong(that.currentSongIndex);
            }
        });
        // save end

        var background = this.add.image(0, 0, "background");
        background.setOrigin(0, 0);

        this.textCurrentSong = this.add.text(158, 116, "", { font: "16px PressStart2P-Regular", align: "center" });
        this.textCurrentSong.setOrigin(0.5, 0.5);

        this.textCurrentSongSubtitle = this.add.text(158, 134, "", { font: "10px PressStart2P-Regular", align: "center" });
        this.textCurrentSongSubtitle.setOrigin(0.5, 0.5);

        this.songs = [this.sound.add("song_1"), this.sound.add("song_2"), this.sound.add("song_3"), this.sound.add("song_4"), this.sound.add("song_5")];

        // this.currentSongIndex = 0;
        // this.playSong(this.currentSongIndex);

        this.input.keyboard.on(
            "keydown_LEFT",
            function () {
                this.switchToPreviousSong();
            },
            this
        );
        this.input.keyboard.on(
            "keydown_RIGHT",
            function () {
                this.switchToNextSong();
            },
            this
        );

        this.textStart = this.add.text(158, 168, "START", { font: "14px PressStart2P-Regular", align: "center" });
        this.textStart.setOrigin(0.5, 0.5);

        this.textExit = this.add.text(158, 190, "EXIT", { font: "10px PressStart2P-Regular", align: "center" });
        this.textExit.setOrigin(0.5, 0.5);

        this.switchAction("start");

        this.input.keyboard.on(
            "keydown_UP",
            function () {
                this.swapAction();
            },
            this
        );
        this.input.keyboard.on(
            "keydown_DOWN",
            function () {
                this.swapAction();
            },
            this
        );

        this.input.keyboard.on(
            "keydown_ENTER",
            function () {
                this.performAction(this.currentAction);
            },
            this
        );

        this.input.keyboard.on(
            "keydown_J",
            function () {
                this.performAction(this.currentAction);
            },
            this
        );

        this.input.keyboard.on(
            "keydown_ESC",
            function () {
                this.switchAction("exit");
            },
            this
        );
    }

    update(time, delta) {}

    switchToNextSong() {
        if (this.currentSongIndex >= 4) {
            this.currentSongIndex = 0;
        } else {
            this.currentSongIndex = this.currentSongIndex + 1;
        }
        // console.log("switched to song: " + this.currentSongIndex);
        this.playSong(this.currentSongIndex);
    }

    switchToPreviousSong() {
        if (this.currentSongIndex <= 0) {
            this.currentSongIndex = 4;
        } else {
            this.currentSongIndex = this.currentSongIndex - 1;
        }
        // console.log("switched to song: " + this.currentSongIndex);
        this.playSong(this.currentSongIndex);
    }

    stopSong() {
        this.songs.forEach((song) => {
            if (song.isPlaying) {
                song.stop();
            }
        });
    }

    playSong(index) {
        this.stopSong();
        if (index >= 0 && index <= 19) {
            this.songs[index].play();

            switch (index) {
                case 0:
                    this.textCurrentSong.setText("Rhythmortis");
                    this.textCurrentSongSubtitle.setText("(Lobby Remix)");
                    break;
                case 1:
                    this.textCurrentSong.setText("Disco Descent");
                    this.textCurrentSongSubtitle.setText("(1-1 Remix)");
                    break;
                case 2:
                    this.textCurrentSong.setText("Crypteque");
                    this.textCurrentSongSubtitle.setText("(1-2 Remix)");
                    break;
                case 3:
                    this.textCurrentSong.setText("Mauseoleum Mash");
                    this.textCurrentSongSubtitle.setText("(1-3 Remix)");
                    break;
                case 4:
                    this.textCurrentSong.setText("Konga Conga Kappa");
                    this.textCurrentSongSubtitle.setText("(King Konga Remix)");
                    break;
            }
        }
    }

    switchAction(action = "start") {
        this.textStart.setColor("#808080");
        this.textExit.setColor("#808080");

        this.textStart.setFontSize(10);
        this.textExit.setFontSize(10);

        switch (action) {
            case "start":
                this.currentAction = 0;
                this.textStart.setColor("#ffffff");
                this.textStart.setFontSize(14);
                break;
            case "exit":
                this.currentAction = 1;
                this.textExit.setColor("#ffffff");
                this.textExit.setFontSize(14);
                break;
        }
    }

    swapAction() {
        if (this.currentAction == 0) {
            this.switchAction("exit");
        } else {
            this.switchAction("start");
        }
    }

    performAction(action) {
        switch (action) {
            case 0:
                this.save();
                this.stopSong();
                switch (this.currentSongIndex) {
                    case 0:
                        this.scene.start("Song1");
                        break;
                    case 1:
                        this.scene.start("Song2");
                        break;
                    case 2:
                        this.scene.start("Song3");
                        break;
                    case 3:
                        this.scene.start("Song4");
                        break;
                    case 4:
                        this.scene.start("Song5");
                        break;
                }
                break;
            case 1:
                nw.App.quit();
                break;
        }
    }

    save() {
        // save start count
        var that = this;

        const fs = require("fs");
        fs.readFile("./saves/main_menu.json", function (err, data) {
            if (err) {
                // create save file if not exist
                let outfile = fs.createWriteStream("./saves/main_menu.json");
                outfile.write(JSON.stringify({ index: that.currentSongIndex }));
                outfile.end();
            } else {
                // load from save file
                let outfile = fs.createWriteStream("./saves/main_menu.json");
                outfile.write(JSON.stringify({ index: that.currentSongIndex }));
                outfile.end();
            }
        });
        // save end
    }
}
