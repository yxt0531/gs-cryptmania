class Song5 extends Phaser.Scene {
    constructor() {
        super({ key: "Song5" });

        var sceneStartTime;
        var sceneCurrentTime;

        var songStartTime;
        var songCurrentTime;
        var songCurrentProgress;

        var song;

        var textStartCountdown;

        var spriteHighlightStrip0;
        var spriteHighlightStrip1;
        var spriteHighlightStrip2;
        var spriteHighlightStrip3;
        var spriteHighlightStrip4;
        var spriteHighlightStrip5;

        // input handler
        var key0;
        var key1;
        var key2;
        var key3;
        var key4;
        var key5;
        // input handler end

        var blockTimestamps;
        var activeBlocks; // current active blocks

        var noteTrigger0;
        var noteTrigger1;
        var noteTrigger2;
        var noteTrigger3;
        var noteTrigger4;
        var noteTrigger5;

        // score and combo
        var score;
        var textScore;
        var textStatus;
        var combo;
        var countDownVisibleTimer;

        var speed;
    }

    preload() {
        this.load.image("background_gameplay", "/game/assets/backgrounds/gameplay.png");
        this.load.audio("song", ["/game/assets/jake_kaufman/crypt_of_the_necrodancer_-_freestyle_retro/05_konga_conga_kappa_(king_konga_remix).ogg"]);

        this.load.image("kp_highlight_strip", "/game/assets/sprites/keypress_highlight_strip.png");

        this.load.image("note_block_pink", "/game/assets/sprites/note_block_pink.png");
        this.load.image("note_block_blue", "/game/assets/sprites/note_block_blue.png");

        this.load.image("note_trigger", "/game/assets/triggers/note_trigger.png");
    }

    create() {
        var background = this.add.image(0, 0, "background_gameplay");
        background.setOrigin(0, 0);

        this.input.keyboard.on(
            "keydown_ESC",
            function () {
                // location.href = "/game/index.html";
                this.song.stop();
                location.href = "/game/index.html";
                // this.scene.start("MainMenu");
            },
            this
        );

        this.sceneStartTime = 0;
        this.sceneCurrentTime = 0;

        this.songStartTime = 0;
        this.songCurrentTime = 0;
        this.songCurrentProgress = 0;

        this.song = this.sound.add("song");

        this.textStartCountdown = this.add.text(158, 40, "5", { font: "24px PressStart2P-Regular", align: "center" });
        this.textStartCountdown.setOrigin(0.5, 0.5);

        this.spriteHighlightStrip0 = this.add.image(68, 191, "kp_highlight_strip");
        this.spriteHighlightStrip0.setOrigin(0, 1);
        this.spriteHighlightStrip1 = this.add.image(98, 191, "kp_highlight_strip");
        this.spriteHighlightStrip1.setOrigin(0, 1);
        this.spriteHighlightStrip2 = this.add.image(128, 191, "kp_highlight_strip");
        this.spriteHighlightStrip2.setOrigin(0, 1);
        this.spriteHighlightStrip3 = this.add.image(158, 191, "kp_highlight_strip");
        this.spriteHighlightStrip3.setOrigin(0, 1);
        this.spriteHighlightStrip4 = this.add.image(188, 191, "kp_highlight_strip");
        this.spriteHighlightStrip4.setOrigin(0, 1);
        this.spriteHighlightStrip5 = this.add.image(218, 191, "kp_highlight_strip");
        this.spriteHighlightStrip5.setOrigin(0, 1);

        this.key0 = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.LEFT);
        this.key1 = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.UP);
        this.key2 = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.RIGHT);
        this.key3 = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.U);
        this.key4 = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.I);
        this.key5 = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.K);

        this.noteTrigger0 = this.physics.add.image(68, 198, "note_trigger");
        this.noteTrigger0.setOrigin(0, 1);
        this.noteTrigger0.setImmovable(true);
        this.noteTrigger1 = this.physics.add.image(98, 198, "note_trigger");
        this.noteTrigger1.setOrigin(0, 1);
        this.noteTrigger1.setImmovable(true);
        this.noteTrigger2 = this.physics.add.image(128, 198, "note_trigger");
        this.noteTrigger2.setOrigin(0, 1);
        this.noteTrigger2.setImmovable(true);
        this.noteTrigger3 = this.physics.add.image(158, 198, "note_trigger");
        this.noteTrigger3.setOrigin(0, 1);
        this.noteTrigger3.setImmovable(true);
        this.noteTrigger4 = this.physics.add.image(188, 198, "note_trigger");
        this.noteTrigger4.setOrigin(0, 1);
        this.noteTrigger4.setImmovable(true);
        this.noteTrigger5 = this.physics.add.image(218, 198, "note_trigger");
        this.noteTrigger5.setOrigin(0, 1);
        this.noteTrigger5.setImmovable(true);

        this.score = 0;
        this.textScore = this.add.text(158, 40, "0", { font: "16px PressStart2P-Regular", align: "center" });
        this.textScore.setOrigin(0.5, 0.5);
        this.textScore.visible = false;
        this.textStatus = this.add.text(158, 58, "PERFECT", { font: "12px PressStart2P-Regular", align: "center" });
        this.textStatus.setOrigin(0.5, 0.5);
        this.textStatus.visible = false;
        this.combo = 0;
        this.countDownVisibleTimer = 1;

        this.speed = 1.5;

        this.input.keyboard.on(
            "keydown_SPACE",
            function () {
                // slow down speed
                if (this.speed == 1.5) {
                    this.speed = 1;
                } else if (this.speed == 2) {
                    this.speed = 1.5;
                } else if (this.speed == 2.5) {
                    this.speed = 2;
                }
                // console.log(this.speed);
            },
            this
        );

        this.input.keyboard.on(
            "keydown_ENTER",
            function () {
                // increase speed
                if (this.speed == 1) {
                    this.speed = 1.5;
                } else if (this.speed == 1.5) {
                    this.speed = 2;
                } else if (this.speed == 2) {
                    this.speed = 2.5;
                }
                // console.log(this.speed);
            },
            this
        );

        // block spawn lanes and times
        // TODO: add notes to current song
        this.blockTimestamps = [
            { lane: 4, time: 4.047980000003008 },
            { lane: 3, time: 4.431465000001481 },
            { lane: 4, time: 4.798490000001038 },
            { lane: 2, time: 5.065435000000434 },
            { lane: 1, time: 5.415840000001481 },
            { lane: 2, time: 5.7661299999999756 },
            { lane: 4, time: 6.049740000002203 },
            { lane: 3, time: 6.433485000001383 },
            { lane: 4, time: 6.783910000001924 },
            { lane: 3, time: 7.017405000002327 },
            { lane: 0, time: 8.051915000000008 },
            { lane: 1, time: 8.418929999999818 },
            { lane: 2, time: 8.752470000003086 },
            { lane: 3, time: 9.052725000001374 },
            { lane: 4, time: 9.586645000003045 },
            { lane: 4, time: 10.07040500000221 },
            { lane: 3, time: 10.53756500000236 },
            { lane: 2, time: 10.804460000003019 },
            { lane: 1, time: 11.03805000000284 },
            { lane: 2, time: 11.521865000002435 },
            { lane: 5, time: 12.039029999999912 },
            { lane: 4, time: 12.573010000000068 },
            { lane: 5, time: 13.006795000001148 },
            { lane: 4, time: 13.37389500000063 },
            { lane: 3, time: 13.724065000002156 },
            { lane: 2, time: 13.99098000000231 },
            { lane: 1, time: 14.374760000002425 },
            { lane: 2, time: 14.725080000000162 },
            { lane: 0, time: 14.97528500000044 },
            { lane: 2, time: 16.026425000000017 },
            { lane: 2, time: 16.543620000000374 },
            { lane: 2, time: 17.010740000001533 },
            { lane: 2, time: 17.49445000000196 },
            { lane: 1, time: 18.011665000001813 },
            { lane: 1, time: 18.545540000002802 },
            { lane: 4, time: 19.046014999999898 },
            { lane: 4, time: 19.496500000001106 },
            { lane: 3, time: 19.98027500000171 },
            { lane: 4, time: 20.514145000001008 },
            { lane: 3, time: 20.997910000001866 },
            { lane: 4, time: 21.53184500000134 },
            { lane: 5, time: 22.03228500000114 },
            { lane: 4, time: 22.532764999999927 },
            { lane: 3, time: 23.016625000000204 },
            { lane: 4, time: 23.5004050000025 },
            { lane: 2, time: 24.067645000002813 },
            { lane: 2, time: 24.53477500000008 },
            { lane: 2, time: 25.05210000000079 },
            { lane: 1, time: 25.519105000003037 },
            { lane: 2, time: 25.752700000000914 },
            { lane: 3, time: 26.002880000000005 },
            { lane: 3, time: 26.52007500000036 },
            { lane: 3, time: 27.020660000001953 },
            { lane: 4, time: 27.487680000002 },
            { lane: 5, time: 27.73806500000137 },
            { lane: 4, time: 28.021609999999782 },
            { lane: 3, time: 28.53877000000284 },
            { lane: 2, time: 29.039275000002817 },
            { lane: 3, time: 29.556415000002744 },
            { lane: 1, time: 30.073820000001433 },
            { lane: 2, time: 30.574094999999943 },
            { lane: 0, time: 31.074595000001864 },
            { lane: 1, time: 31.324910000003 },
            { lane: 2, time: 31.57515500000227 },
            { lane: 2, time: 32.05889500000194 },
            { lane: 2, time: 32.55942000000141 },
            { lane: 3, time: 33.043274999999994 },
            { lane: 3, time: 33.54375000000073 },
            { lane: 2, time: 34.04422500000146 },
            { lane: 2, time: 34.561390000002575 },
            { lane: 4, time: 35.04529000000184 },
            { lane: 4, time: 35.56239500000083 },
            { lane: 2, time: 36.04622999999992 },
            { lane: 2, time: 36.563475000002654 },
            { lane: 5, time: 37.03053500000169 },
            { lane: 5, time: 37.5143250000001 },
            { lane: 1, time: 38.0815700000021 },
            { lane: 1, time: 38.59878000000026 },
            { lane: 4, time: 39.082570000002306 },
            { lane: 4, time: 39.56635500000266 },
            { lane: 0, time: 40.06689000000188 },
            { lane: 0, time: 40.53398500000185 },
            { lane: 3, time: 41.08452500000203 },
            { lane: 2, time: 41.635095000001456 },
            { lane: 4, time: 42.085549999999785 },
            { lane: 1, time: 42.5527800000018 },
            { lane: 2, time: 43.01985500000228 },
            { lane: 3, time: 43.53697500000271 },
            { lane: 4, time: 44.070895000000746 },
            { lane: 5, time: 44.554690000000846 },
            { lane: 2, time: 45.10523000000103 },
            { lane: 3, time: 45.57237499999974 },
            { lane: 1, time: 46.05616000000009 },
            { lane: 4, time: 46.40650000000096 },
            { lane: 2, time: 46.756900000000314 },
            { lane: 2, time: 46.756900000000314 },
            { lane: 3, time: 46.756900000000314 },
        ];
        this.activeBlocks = [];
    }

    getBlockSpawnYVelocity(speed = 1) {
        return speed * 96; // default speed is 96
    }

    getBlockSpawnYPosition(speed = 1) {
        return -((speed - 1) * 192); // default travel distance is 192
    }

    spawnBlock(lane, speed) {
        var that = this;
        if (lane >= 0 && lane <= 5) {
            var newBlock;
            switch (lane) {
                case 0:
                    newBlock = this.physics.add.image(68 + 14.5, this.getBlockSpawnYPosition(speed), "note_block_pink");
                    this.physics.add.overlap(newBlock, this.noteTrigger0, function () {
                        if (that.key0.isDown && newBlock.visible) {
                            newBlock.visible = false;
                            if (newBlock.y < 178) {
                                // console.log("missed"); // missed by early press
                                that.textStatus.setText("MISSED");
                                that.textStatus.visible = true;
                                that.countDownVisibleTimer = 1;
                                that.combo = 0;
                            } else {
                                if (newBlock.y >= 185 && newBlock.y <= 202) {
                                    // console.log("perfect"); // perfect hit
                                    that.score = that.score + (10 + 5 * (that.combo / 10));
                                    that.combo = that.combo + 1;

                                    that.textStatus.setText("PERFECT");
                                    that.textStatus.visible = true;
                                    that.countDownVisibleTimer = 1;
                                } else {
                                    // console.log("good"); // good hit
                                    that.score = that.score + (8 + 5 * (that.combo / 10));
                                    that.combo = that.combo + 1;

                                    that.textStatus.setText("GOOD");
                                    that.textStatus.visible = true;
                                    that.countDownVisibleTimer = 1;
                                }
                            }
                        }
                    });
                    break;
                case 1:
                    newBlock = this.physics.add.image(98 + 14.5, this.getBlockSpawnYPosition(speed), "note_block_blue");
                    this.physics.add.overlap(newBlock, this.noteTrigger1, function () {
                        if (that.key1.isDown && newBlock.visible) {
                            newBlock.visible = false;
                            if (newBlock.y < 178) {
                                that.textStatus.setText("MISSED");
                                that.textStatus.visible = true;
                                that.countDownVisibleTimer = 1;
                                that.combo = 0;
                            } else {
                                if (newBlock.y >= 185 && newBlock.y <= 202) {
                                    that.score = that.score + (10 + 5 * (that.combo / 10));
                                    that.combo = that.combo + 1;

                                    that.textStatus.setText("PERFECT");
                                    that.textStatus.visible = true;
                                    that.countDownVisibleTimer = 1;
                                } else {
                                    that.score = that.score + (8 + 5 * (that.combo / 10));
                                    that.combo = that.combo + 1;

                                    that.textStatus.setText("GOOD");
                                    that.textStatus.visible = true;
                                    that.countDownVisibleTimer = 1;
                                }
                            }
                        }
                    });
                    break;
                case 2:
                    newBlock = this.physics.add.image(128 + 14.5, this.getBlockSpawnYPosition(speed), "note_block_pink");
                    this.physics.add.overlap(newBlock, this.noteTrigger2, function () {
                        if (that.key2.isDown && newBlock.visible) {
                            newBlock.visible = false;
                            if (newBlock.y < 178) {
                                that.textStatus.setText("MISSED");
                                that.textStatus.visible = true;
                                that.countDownVisibleTimer = 1;
                                that.combo = 0;
                            } else {
                                if (newBlock.y >= 185 && newBlock.y <= 202) {
                                    that.score = that.score + (10 + 5 * (that.combo / 10));
                                    that.combo = that.combo + 1;

                                    that.textStatus.setText("PERFECT");
                                    that.textStatus.visible = true;
                                    that.countDownVisibleTimer = 1;
                                } else {
                                    that.score = that.score + (8 + 5 * (that.combo / 10));
                                    that.combo = that.combo + 1;

                                    that.textStatus.setText("GOOD");
                                    that.textStatus.visible = true;
                                    that.countDownVisibleTimer = 1;
                                }
                            }
                        }
                    });
                    break;
                case 3:
                    newBlock = this.physics.add.image(158 + 14.5, this.getBlockSpawnYPosition(speed), "note_block_pink");
                    this.physics.add.overlap(newBlock, this.noteTrigger3, function () {
                        if (that.key3.isDown && newBlock.visible) {
                            newBlock.visible = false;
                            if (newBlock.y < 178) {
                                that.textStatus.setText("MISSED");
                                that.textStatus.visible = true;
                                that.countDownVisibleTimer = 1;
                                that.combo = 0;
                            } else {
                                if (newBlock.y >= 185 && newBlock.y <= 202) {
                                    that.score = that.score + (10 + 5 * (that.combo / 10));
                                    that.combo = that.combo + 1;

                                    that.textStatus.setText("PERFECT");
                                    that.textStatus.visible = true;
                                    that.countDownVisibleTimer = 1;
                                } else {
                                    that.score = that.score + (8 + 5 * (that.combo / 10));
                                    that.combo = that.combo + 1;

                                    that.textStatus.setText("GOOD");
                                    that.textStatus.visible = true;
                                    that.countDownVisibleTimer = 1;
                                }
                            }
                        }
                    });
                    break;
                case 4:
                    newBlock = this.physics.add.image(188 + 14.5, this.getBlockSpawnYPosition(speed), "note_block_blue");
                    this.physics.add.overlap(newBlock, this.noteTrigger4, function () {
                        if (that.key4.isDown && newBlock.visible) {
                            newBlock.visible = false;
                            if (newBlock.y < 178) {
                                that.textStatus.setText("MISSED");
                                that.textStatus.visible = true;
                                that.countDownVisibleTimer = 1;
                                that.combo = 0;
                            } else {
                                if (newBlock.y >= 185 && newBlock.y <= 202) {
                                    that.score = that.score + (10 + 5 * (that.combo / 10));
                                    that.combo = that.combo + 1;

                                    that.textStatus.setText("PERFECT");
                                    that.textStatus.visible = true;
                                    that.countDownVisibleTimer = 1;
                                } else {
                                    that.score = that.score + (8 + 5 * (that.combo / 10));
                                    that.combo = that.combo + 1;

                                    that.textStatus.setText("GOOD");
                                    that.textStatus.visible = true;
                                    that.countDownVisibleTimer = 1;
                                }
                            }
                        }
                    });
                    break;
                case 5:
                    newBlock = this.physics.add.image(218 + 14.5, this.getBlockSpawnYPosition(speed), "note_block_pink");
                    this.physics.add.overlap(newBlock, this.noteTrigger5, function () {
                        if (that.key5.isDown && newBlock.visible) {
                            newBlock.visible = false;
                            if (newBlock.y < 178) {
                                that.textStatus.setText("MISSED");
                                that.textStatus.visible = true;
                                that.countDownVisibleTimer = 1;
                                that.combo = 0;
                            } else {
                                if (newBlock.y >= 185 && newBlock.y <= 202) {
                                    that.score = that.score + (10 + 5 * (that.combo / 10));
                                    that.combo = that.combo + 1;

                                    that.textStatus.setText("PERFECT");
                                    that.textStatus.visible = true;
                                    that.countDownVisibleTimer = 1;
                                } else {
                                    that.score = that.score + (8 + 5 * (that.combo / 10));
                                    that.combo = that.combo + 1;

                                    that.textStatus.setText("GOOD");
                                    that.textStatus.visible = true;
                                    that.countDownVisibleTimer = 1;
                                }
                            }
                        }
                    });
                    break;
            }
            newBlock.setOrigin(0, 0.5);
            newBlock.setVelocityY(this.getBlockSpawnYVelocity(speed));

            this.activeBlocks.push(newBlock);
        }
    }

    update(time, delta) {
        if (this.sceneStartTime == 0) {
            this.sceneStartTime = time;
        }
        this.sceneCurrentTime = time - this.sceneStartTime;
        if (this.sceneCurrentTime < 5000) {
            this.textStartCountdown.setText(5 - (this.sceneCurrentTime / 1000).toFixed(0));
        } else if (this.sceneCurrentTime >= 5000 && !this.song.isPlaying && this.songCurrentProgress == 0) {
            // song starts here
            this.textStartCountdown.visible = false;
            this.song.play();
            this.songStartTime = this.sceneCurrentTime;
            this.textScore.visible = true;
        } else if (this.song.isPlaying) {
            // song playing here
            this.songCurrentTime = (this.sceneCurrentTime - this.songStartTime) / 1000; // in second
            var remainingTime = this.song.duration + this.sceneCurrentTime / 1000 - (this.songCurrentTime + this.sceneCurrentTime / 1000); // in second
            this.songCurrentProgress = 1 - remainingTime / this.song.duration; // 0 - 1

            // spawn blocks here
            this.blockTimestamps.forEach((block) => {
                if (this.songCurrentTime >= block.time - 2.1) {
                    this.spawnBlock(block.lane, this.speed); // set spawn speed here
                    block.lane = -1;
                    block.time = this.song.duration * 255;
                }
            });

            // remove active blocks here
            for (var i = 0; i < this.activeBlocks.length; i++) {
                if (this.activeBlocks[i].y >= 203) {
                    if (this.activeBlocks[i].visible) {
                        // console.log("missed"); // missed by late press
                        this.combo = 0;

                        this.textStatus.setText("MISSED");
                        this.textStatus.visible = true;
                        this.countDownVisibleTimer = 1;
                    }
                    this.activeBlocks[i].destroy();
                    this.activeBlocks.splice(i, 1);
                }
            }

            this.textScore.setText(this.score.toFixed(0));

            if (this.countDownVisibleTimer > 0) {
                this.countDownVisibleTimer = this.countDownVisibleTimer - delta / 1000;
            } else {
                this.textStatus.visible = false;
            }
        } else {
            // song end
            this.textStatus.setText("Press MENU to Return");
            this.textStatus.y = 90;
            this.textStatus.visible = true;
        }

        if (this.key0.isDown) {
            this.spriteHighlightStrip0.visible = true;
        } else {
            this.spriteHighlightStrip0.visible = false;
        }

        if (this.key1.isDown) {
            this.spriteHighlightStrip1.visible = true;
        } else {
            this.spriteHighlightStrip1.visible = false;
        }

        if (this.key2.isDown) {
            this.spriteHighlightStrip2.visible = true;
        } else {
            this.spriteHighlightStrip2.visible = false;
        }

        if (this.key3.isDown) {
            this.spriteHighlightStrip3.visible = true;
        } else {
            this.spriteHighlightStrip3.visible = false;
        }

        if (this.key4.isDown) {
            this.spriteHighlightStrip4.visible = true;
        } else {
            this.spriteHighlightStrip4.visible = false;
        }

        if (this.key5.isDown) {
            this.spriteHighlightStrip5.visible = true;
        } else {
            this.spriteHighlightStrip5.visible = false;
        }
    }
}
