class Song4 extends Phaser.Scene {
    constructor() {
        super({ key: "Song4" });

        var sceneStartTime;
        var sceneCurrentTime;

        var songStartTime;
        var songCurrentTime;
        var songCurrentProgress;

        var song;

        var textStartCountdown;

        var spriteHighlightStrip0;
        var spriteHighlightStrip1;
        var spriteHighlightStrip2;
        var spriteHighlightStrip3;
        var spriteHighlightStrip4;
        var spriteHighlightStrip5;

        // input handler
        var key0;
        var key1;
        var key2;
        var key3;
        var key4;
        var key5;
        // input handler end

        var blockTimestamps;
        var activeBlocks; // current active blocks

        var noteTrigger0;
        var noteTrigger1;
        var noteTrigger2;
        var noteTrigger3;
        var noteTrigger4;
        var noteTrigger5;

        // score and combo
        var score;
        var textScore;
        var textStatus;
        var combo;
        var countDownVisibleTimer;

        var speed;
    }

    preload() {
        this.load.image("background_gameplay", "/game/assets/backgrounds/gameplay.png");
        this.load.audio("song", ["/game/assets/jake_kaufman/crypt_of_the_necrodancer_-_freestyle_retro/04_mauseoleum_mash_(1-3_remix).ogg"]);

        this.load.image("kp_highlight_strip", "/game/assets/sprites/keypress_highlight_strip.png");

        this.load.image("note_block_pink", "/game/assets/sprites/note_block_pink.png");
        this.load.image("note_block_blue", "/game/assets/sprites/note_block_blue.png");

        this.load.image("note_trigger", "/game/assets/triggers/note_trigger.png");
    }

    create() {
        var background = this.add.image(0, 0, "background_gameplay");
        background.setOrigin(0, 0);

        this.input.keyboard.on(
            "keydown_ESC",
            function () {
                // location.href = "/game/index.html";
                this.song.stop();
                location.href = "/game/index.html";
                // this.scene.start("MainMenu");
            },
            this
        );

        this.sceneStartTime = 0;
        this.sceneCurrentTime = 0;

        this.songStartTime = 0;
        this.songCurrentTime = 0;
        this.songCurrentProgress = 0;

        this.song = this.sound.add("song");

        this.textStartCountdown = this.add.text(158, 40, "5", { font: "24px PressStart2P-Regular", align: "center" });
        this.textStartCountdown.setOrigin(0.5, 0.5);

        this.spriteHighlightStrip0 = this.add.image(68, 191, "kp_highlight_strip");
        this.spriteHighlightStrip0.setOrigin(0, 1);
        this.spriteHighlightStrip1 = this.add.image(98, 191, "kp_highlight_strip");
        this.spriteHighlightStrip1.setOrigin(0, 1);
        this.spriteHighlightStrip2 = this.add.image(128, 191, "kp_highlight_strip");
        this.spriteHighlightStrip2.setOrigin(0, 1);
        this.spriteHighlightStrip3 = this.add.image(158, 191, "kp_highlight_strip");
        this.spriteHighlightStrip3.setOrigin(0, 1);
        this.spriteHighlightStrip4 = this.add.image(188, 191, "kp_highlight_strip");
        this.spriteHighlightStrip4.setOrigin(0, 1);
        this.spriteHighlightStrip5 = this.add.image(218, 191, "kp_highlight_strip");
        this.spriteHighlightStrip5.setOrigin(0, 1);

        this.key0 = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.LEFT);
        this.key1 = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.UP);
        this.key2 = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.RIGHT);
        this.key3 = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.U);
        this.key4 = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.I);
        this.key5 = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.K);

        this.noteTrigger0 = this.physics.add.image(68, 198, "note_trigger");
        this.noteTrigger0.setOrigin(0, 1);
        this.noteTrigger0.setImmovable(true);
        this.noteTrigger1 = this.physics.add.image(98, 198, "note_trigger");
        this.noteTrigger1.setOrigin(0, 1);
        this.noteTrigger1.setImmovable(true);
        this.noteTrigger2 = this.physics.add.image(128, 198, "note_trigger");
        this.noteTrigger2.setOrigin(0, 1);
        this.noteTrigger2.setImmovable(true);
        this.noteTrigger3 = this.physics.add.image(158, 198, "note_trigger");
        this.noteTrigger3.setOrigin(0, 1);
        this.noteTrigger3.setImmovable(true);
        this.noteTrigger4 = this.physics.add.image(188, 198, "note_trigger");
        this.noteTrigger4.setOrigin(0, 1);
        this.noteTrigger4.setImmovable(true);
        this.noteTrigger5 = this.physics.add.image(218, 198, "note_trigger");
        this.noteTrigger5.setOrigin(0, 1);
        this.noteTrigger5.setImmovable(true);

        this.score = 0;
        this.textScore = this.add.text(158, 40, "0", { font: "16px PressStart2P-Regular", align: "center" });
        this.textScore.setOrigin(0.5, 0.5);
        this.textScore.visible = false;
        this.textStatus = this.add.text(158, 58, "PERFECT", { font: "12px PressStart2P-Regular", align: "center" });
        this.textStatus.setOrigin(0.5, 0.5);
        this.textStatus.visible = false;
        this.combo = 0;
        this.countDownVisibleTimer = 1;

        this.speed = 1.5;

        this.input.keyboard.on(
            "keydown_SPACE",
            function () {
                // slow down speed
                if (this.speed == 1.5) {
                    this.speed = 1;
                } else if (this.speed == 2) {
                    this.speed = 1.5;
                } else if (this.speed == 2.5) {
                    this.speed = 2;
                }
                // console.log(this.speed);
            },
            this
        );

        this.input.keyboard.on(
            "keydown_ENTER",
            function () {
                // increase speed
                if (this.speed == 1) {
                    this.speed = 1.5;
                } else if (this.speed == 1.5) {
                    this.speed = 2;
                } else if (this.speed == 2) {
                    this.speed = 2.5;
                }
                // console.log(this.speed);
            },
            this
        );

        // block spawn lanes and times
        // TODO: add notes to current song
        this.blockTimestamps = [
            { lane: 2, time: 6.882529999998951 },
            { lane: 2, time: 6.882529999998951 },
            { lane: 3, time: 6.882529999998951 },
            { lane: 4, time: 7.733384999999544 },
            { lane: 4, time: 8.200519999998505 },
            { lane: 3, time: 8.634439999997994 },
            { lane: 4, time: 9.051479999998264 },
            { lane: 4, time: 9.418374999997468 },
            { lane: 4, time: 9.885549999999057 },
            { lane: 1, time: 10.28624999999738 },
            { lane: 2, time: 10.753014999998413 },
            { lane: 2, time: 11.22016999999687 },
            { lane: 2, time: 11.637244999998074 },
            { lane: 1, time: 12.054290000000037 },
            { lane: 2, time: 12.438060000000405 },
            { lane: 3, time: 12.905139999998937 },
            { lane: 4, time: 13.305649999998423 },
            { lane: 5, time: 13.756040000000212 },
            { lane: 0, time: 13.77268499999991 },
            { lane: 1, time: 14.173189999997703 },
            { lane: 2, time: 14.339974999998958 },
            { lane: 3, time: 14.573499999998603 },
            { lane: 4, time: 15.023929999999382 },
            { lane: 3, time: 15.441025000000081 },
            { lane: 2, time: 15.641224999999395 },
            { lane: 1, time: 15.841394999999466 },
            { lane: 3, time: 16.29195499999696 },
            { lane: 4, time: 16.70900999999867 },
            { lane: 1, time: 17.17612999999983 },
            { lane: 1, time: 17.17612999999983 },
            { lane: 4, time: 17.17612999999983 },
            { lane: 3, time: 17.57650999999896 },
            { lane: 2, time: 17.743319999997766 },
            { lane: 4, time: 17.993619999997463 },
            { lane: 5, time: 18.44400499999756 },
            { lane: 0, time: 18.82772499999919 },
            { lane: 1, time: 19.044565000000148 },
            { lane: 2, time: 19.24492999999711 },
            { lane: 3, time: 19.44553999999698 },
            { lane: 4, time: 19.645274999998946 },
            { lane: 5, time: 19.87878499999715 },
            { lane: 4, time: 20.095659999999043 },
            { lane: 3, time: 20.312819999999192 },
            { lane: 2, time: 20.54620999999679 },
            { lane: 3, time: 20.56288499999937 },
            { lane: 1, time: 21.013359999997192 },
            { lane: 2, time: 21.4637049999983 },
            { lane: 3, time: 21.864144999999553 },
            { lane: 1, time: 22.09764499999801 },
            { lane: 4, time: 22.281134999997448 },
            { lane: 5, time: 22.73156999999992 },
            { lane: 4, time: 23.182064999997237 },
            { lane: 3, time: 23.5824649999995 },
            { lane: 0, time: 24.016270000000077 },
            { lane: 1, time: 24.4499599999981 },
            { lane: 2, time: 24.900415000000066 },
            { lane: 3, time: 25.300830000000133 },
            { lane: 4, time: 25.71788000000015 },
            { lane: 3, time: 26.168555000000197 },
            { lane: 5, time: 26.60224499999822 },
            { lane: 4, time: 26.80231499999718 },
            { lane: 3, time: 27.002590000000055 },
            { lane: 2, time: 27.20269499999995 },
            { lane: 1, time: 27.453034999998636 },
            { lane: 2, time: 27.88669999999911 },
            { lane: 3, time: 28.320514999999432 },
            { lane: 4, time: 28.720924999997806 },
            { lane: 5, time: 29.15460499999972 },
            { lane: 4, time: 29.605129999999917 },
            { lane: 0, time: 30.022134999999253 },
            { lane: 1, time: 30.472584999999526 },
            { lane: 2, time: 30.856294999997772 },
            { lane: 3, time: 31.32346999999936 },
            { lane: 1, time: 31.773914999997942 },
            { lane: 2, time: 32.190999999998894 },
            { lane: 5, time: 32.59180999999808 },
            { lane: 4, time: 33.04179999999906 },
            { lane: 3, time: 33.45903499999986 },
            { lane: 2, time: 33.89278999999806 },
            { lane: 1, time: 34.27639999999883 },
            { lane: 2, time: 34.74353499999779 },
            { lane: 3, time: 35.160604999997304 },
            { lane: 4, time: 35.611039999999775 },
            { lane: 0, time: 36.02814500000022 },
            { lane: 1, time: 36.24518999999782 },
            { lane: 2, time: 36.4453199999989 },
            { lane: 3, time: 36.87911499999973 },
            { lane: 4, time: 37.32941999999821 },
            { lane: 5, time: 37.72980499999903 },
            { lane: 4, time: 37.946734999997716 },
            { lane: 3, time: 38.163659999998345 },
            { lane: 2, time: 38.63078499999756 },
            { lane: 1, time: 39.0311249999977 },
            { lane: 2, time: 39.464899999999034 },
            { lane: 3, time: 39.915344999997615 },
            { lane: 4, time: 40.33236499999839 },
            { lane: 5, time: 40.76617499999702 },
            { lane: 4, time: 41.199955000000045 },
            { lane: 1, time: 41.61701499999981 },
            { lane: 2, time: 42.03410499999882 },
            { lane: 3, time: 42.46789499999795 },
            { lane: 0, time: 42.90167499999734 },
            { lane: 1, time: 43.318709999999555 },
            { lane: 2, time: 43.73579499999687 },
            { lane: 3, time: 44.15285999999833 },
            { lane: 1, time: 44.586615000000165 },
            { lane: 2, time: 45.03714499999842 },
            { lane: 2, time: 45.4541449999997 },
            { lane: 3, time: 45.88794499999858 },
            { lane: 4, time: 46.30496999999741 },
            { lane: 5, time: 46.72209499999735 },
            { lane: 4, time: 47.172559999999066 },
            { lane: 3, time: 47.60627999999997 },
            { lane: 2, time: 48.00673999999708 },
            { lane: 1, time: 48.4571849999993 },
            { lane: 2, time: 48.89087499999732 },
            { lane: 3, time: 49.30798499999946 },
            { lane: 2, time: 49.74173999999766 },
            { lane: 4, time: 50.17558499999723 },
            { lane: 5, time: 50.592579999996815 },
            { lane: 4, time: 51.026324999998906 },
            { lane: 0, time: 51.443514999999024 },
            { lane: 1, time: 51.87720999999874 },
            { lane: 2, time: 52.29425999999876 },
            { lane: 3, time: 52.74490499999956 },
            { lane: 4, time: 53.1618899999994 },
            { lane: 5, time: 53.578864999999496 },
            { lane: 4, time: 53.77909499999805 },
            { lane: 3, time: 53.99597999999969 },
            { lane: 2, time: 54.22952499999883 },
            { lane: 1, time: 54.44640499999878 },
            { lane: 2, time: 54.64662999999928 },
            { lane: 3, time: 54.86346499999854 },
            { lane: 4, time: 55.28061499999967 },
            { lane: 5, time: 55.714319999999134 },
            { lane: 4, time: 56.13145499999882 },
            { lane: 0, time: 56.581914999998844 },
            { lane: 1, time: 56.79873999999836 },
            { lane: 2, time: 56.999009999999544 },
            { lane: 3, time: 57.23253499999919 },
            { lane: 4, time: 57.46612499999901 },
            { lane: 5, time: 57.64984999999797 },
            { lane: 4, time: 57.84985999999844 },
            { lane: 3, time: 58.05005999999776 },
            { lane: 1, time: 58.283574999997654 },
            { lane: 4, time: 58.75067499999932 },
            { lane: 2, time: 58.90085999999792 },
            { lane: 3, time: 59.16774499999883 },
            { lane: 1, time: 59.367959999999584 },
            { lane: 4, time: 59.60157999999865 },
            { lane: 1, time: 59.8184399999991 },
            { lane: 5, time: 60.05198499999824 },
            { lane: 0, time: 60.48570499999914 },
            { lane: 1, time: 60.90291999999681 },
            { lane: 2, time: 61.31988499999716 },
            { lane: 3, time: 61.720330000000104 },
            { lane: 4, time: 62.170734999999695 },
            { lane: 5, time: 62.571179999999 },
            { lane: 4, time: 63.004939999998896 },
            { lane: 1, time: 63.472194999998464 },
            { lane: 2, time: 63.63896499999828 },
            { lane: 3, time: 63.855744999997114 },
            { lane: 4, time: 64.30618999999933 },
            { lane: 4, time: 64.7566599999991 },
            { lane: 3, time: 64.85679999999775 },
            { lane: 2, time: 65.19054499999766 },
            { lane: 1, time: 65.6241749999972 },
            { lane: 2, time: 66.05794999999853 },
            { lane: 3, time: 66.49169999999867 },
            { lane: 4, time: 66.67523499999879 },
            { lane: 5, time: 66.87548999999854 },
            { lane: 0, time: 67.29249999999956 },
            { lane: 1, time: 67.54292999999961 },
            { lane: 2, time: 67.74293000000034 },
            { lane: 4, time: 67.95991999999751 },
            { lane: 1, time: 68.16005999999834 },
            { lane: 3, time: 68.39365999999791 },
            { lane: 5, time: 68.62720499999705 },
            { lane: 4, time: 69.06094499999745 },
            { lane: 3, time: 69.26122000000032 },
            { lane: 2, time: 69.46128499999759 },
            { lane: 1, time: 69.87845000000016 },
            { lane: 2, time: 70.32881999999881 },
            { lane: 3, time: 70.7460949999986 },
            { lane: 4, time: 70.91289499999766 },
            { lane: 5, time: 71.11295499999687 },
            { lane: 4, time: 71.34654999999839 },
            { lane: 3, time: 71.56346999999732 },
            { lane: 1, time: 71.76362999999765 },
            { lane: 2, time: 71.9804750000003 },
            { lane: 0, time: 72.41424499999994 },
            { lane: 1, time: 72.84803499999907 },
            { lane: 2, time: 73.31516499999998 },
            { lane: 4, time: 73.71561499999734 },
            { lane: 5, time: 73.91575499999817 },
            { lane: 4, time: 74.11599999999817 },
            { lane: 3, time: 74.33284999999887 },
            { lane: 2, time: 74.54967499999839 },
            { lane: 1, time: 74.78325499999846 },
            { lane: 2, time: 74.96678499999689 },
            { lane: 3, time: 75.21764499999699 },
            { lane: 4, time: 75.41719999999987 },
            { lane: 5, time: 75.86766999999963 },
            { lane: 4, time: 76.30157499999768 },
            { lane: 3, time: 76.70181500000035 },
            { lane: 2, time: 77.13564000000042 },
            { lane: 1, time: 77.58626000000004 },
            { lane: 0, time: 77.98648999999932 },
            { lane: 1, time: 78.42019499999878 },
            { lane: 2, time: 78.87069999999949 },
            { lane: 4, time: 79.3378449999982 },
            { lane: 3, time: 79.73819000000003 },
            { lane: 5, time: 80.17193499999848 },
            { lane: 4, time: 80.60575499999686 },
            { lane: 1, time: 81.02279999999882 },
            { lane: 2, time: 81.20630999999776 },
            { lane: 3, time: 81.4231749999999 },
            { lane: 4, time: 81.85698499999853 },
            { lane: 5, time: 82.27404000000024 },
            { lane: 4, time: 82.49093499999799 },
            { lane: 3, time: 82.70779000000039 },
            { lane: 2, time: 82.90799999999945 },
            { lane: 1, time: 83.12499000000025 },
            { lane: 2, time: 83.54196500000035 },
            { lane: 4, time: 84.00914999999804 },
            { lane: 3, time: 84.4428399999997 },
            { lane: 0, time: 84.89333499999702 },
            { lane: 1, time: 85.32706499999767 },
            { lane: 5, time: 85.76081499999782 },
            { lane: 4, time: 85.99438499999815 },
            { lane: 3, time: 86.17789999999877 },
            { lane: 1, time: 86.41152499999953 },
            { lane: 2, time: 86.64510999999766 },
            { lane: 4, time: 87.02917499999967 },
            { lane: 2, time: 87.46255499999825 },
            { lane: 2, time: 87.91295499999978 },
            { lane: 2, time: 88.33006000000023 },
            { lane: 2, time: 88.33006000000023 },
            { lane: 4, time: 88.33006000000023 },
            { lane: 2, time: 88.78060999999798 },
            { lane: 2, time: 88.78060999999798 },
            { lane: 5, time: 88.78060999999798 },
            { lane: 4, time: 88.98071999999956 },
            { lane: 2, time: 88.99738499999876 },
            { lane: 3, time: 89.19757000000027 },
            { lane: 2, time: 89.21424999999726 },
            { lane: 1, time: 89.63136499999746 },
            { lane: 2, time: 90.03169999999955 },
            { lane: 3, time: 90.4655649999986 },
            { lane: 0, time: 90.66572999999698 },
            { lane: 1, time: 90.88257999999769 },
            { lane: 2, time: 91.33302999999796 },
            { lane: 4, time: 91.54992499999935 },
            { lane: 5, time: 91.75020999999833 },
            { lane: 4, time: 92.18395999999848 },
            { lane: 3, time: 92.36742500000037 },
            { lane: 1, time: 92.5843449999993 },
            { lane: 2, time: 92.78445999999894 },
            { lane: 4, time: 93.01803999999902 },
            { lane: 5, time: 93.23493999999846 },
            { lane: 4, time: 93.45176499999798 },
            { lane: 1, time: 93.63534999999683 },
            { lane: 2, time: 93.85218999999779 },
            { lane: 2, time: 94.35269999999946 },
            { lane: 3, time: 94.7530749999969 },
            { lane: 2, time: 95.20358999999735 },
            { lane: 4, time: 95.63726999999926 },
            { lane: 1, time: 96.03772499999832 },
            { lane: 5, time: 96.47148000000016 },
            { lane: 2, time: 96.9052300000003 },
            { lane: 4, time: 97.33909999999742 },
            { lane: 3, time: 97.73937999999907 },
            { lane: 2, time: 98.17321999999695 },
            { lane: 1, time: 98.59031499999764 },
            { lane: 2, time: 99.02406499999779 },
            { lane: 0, time: 99.44134499999927 },
            { lane: 2, time: 99.90818999999829 },
            { lane: 1, time: 100.30861000000004 },
            { lane: 2, time: 100.70902500000011 },
            { lane: 4, time: 101.15949499999988 },
            { lane: 3, time: 101.6099899999972 },
            { lane: 5, time: 102.0102850000003 },
            { lane: 4, time: 102.46082000000024 },
            { lane: 3, time: 102.86118499999793 },
            { lane: 1, time: 103.29540999999881 },
            { lane: 2, time: 103.71201499999734 },
            { lane: 4, time: 104.1791349999985 },
            { lane: 3, time: 104.61289499999839 },
            { lane: 5, time: 105.01328999999896 },
            { lane: 0, time: 105.4470399999991 },
            { lane: 1, time: 105.88080499999705 },
            { lane: 2, time: 106.33131499999945 },
            { lane: 4, time: 106.78171999999904 },
            { lane: 5, time: 107.19878499999686 },
            { lane: 4, time: 107.61589999999705 },
            { lane: 3, time: 108.03294999999707 },
            { lane: 2, time: 108.48338499999954 },
            { lane: 1, time: 108.90053499999703 },
            { lane: 2, time: 109.30084999999963 },
            { lane: 4, time: 109.76796999999715 },
            { lane: 5, time: 110.15169499999683 },
            { lane: 4, time: 110.60215499999686 },
            { lane: 3, time: 111.05265999999756 },
            { lane: 4, time: 111.48640499999965 },
            { lane: 5, time: 111.90347499999916 },
            { lane: 4, time: 112.33729999999923 },
            { lane: 1, time: 112.77101000000039 },
            { lane: 2, time: 113.1880600000004 },
            { lane: 3, time: 113.58847999999853 },
            { lane: 2, time: 114.0389299999988 },
            { lane: 4, time: 114.42263499999899 },
            { lane: 2, time: 114.85640499999863 },
            { lane: 3, time: 115.27350499999739 },
            { lane: 1, time: 115.72388499999943 },
            { lane: 4, time: 116.1577099999995 },
            { lane: 0, time: 116.57478499999706 },
            { lane: 5, time: 116.9919349999982 },
            { lane: 1, time: 117.4256349999996 },
            { lane: 4, time: 117.87604499999725 },
            { lane: 2, time: 118.30981499999689 },
            { lane: 3, time: 118.7602799999986 },
            { lane: 1, time: 119.1439649999993 },
            { lane: 4, time: 119.59445499999856 },
            { lane: 5, time: 120.01153999999951 },
            { lane: 4, time: 120.46197499999835 },
            { lane: 3, time: 120.89576999999917 },
            { lane: 4, time: 121.32948499999839 },
            { lane: 0, time: 121.74658999999883 },
            { lane: 1, time: 122.18039999999746 },
            { lane: 2, time: 122.61418499999854 },
            { lane: 1, time: 122.9978549999978 },
            { lane: 5, time: 123.46492500000022 },
            { lane: 4, time: 123.93214999999691 },
            { lane: 3, time: 124.31576999999743 },
            { lane: 4, time: 124.7495600000002 },
            { lane: 2, time: 125.16666499999701 },
            { lane: 1, time: 125.61708999999973 },
            { lane: 0, time: 126.03423499999917 },
            { lane: 1, time: 126.45139500000005 },
            { lane: 4, time: 126.90165999999954 },
            { lane: 5, time: 127.3021099999969 },
            { lane: 3, time: 127.75252999999793 },
            { lane: 4, time: 128.1863749999975 },
            { lane: 2, time: 128.5700149999975 },
            { lane: 1, time: 129.03719499999715 },
            { lane: 0, time: 129.42090499999904 },
            { lane: 1, time: 129.83804500000042 },
            { lane: 4, time: 130.27174999999988 },
            { lane: 4, time: 130.75555499999973 },
            { lane: 4, time: 131.15587999999843 },
            { lane: 4, time: 131.58973999999944 },
            { lane: 1, time: 132.04013999999734 },
            { lane: 1, time: 132.47397499999715 },
            { lane: 2, time: 132.87424499999906 },
            { lane: 2, time: 133.308009999997 },
            { lane: 3, time: 133.75846999999703 },
            { lane: 3, time: 134.17563999999766 },
            { lane: 3, time: 134.62609999999768 },
            { lane: 3, time: 135.00976999999693 },
            { lane: 5, time: 135.4601999999977 },
            { lane: 5, time: 135.87728499999866 },
            { lane: 5, time: 136.32774499999869 },
            { lane: 5, time: 136.76147499999934 },
            { lane: 4, time: 137.1787149999982 },
            { lane: 4, time: 137.59559499999887 },
            { lane: 4, time: 138.01270999999906 },
            { lane: 4, time: 138.44648999999845 },
            { lane: 1, time: 138.91375499999776 },
            { lane: 1, time: 139.29733999999735 },
            { lane: 2, time: 139.74774499999694 },
            { lane: 2, time: 140.16488000000027 },
            { lane: 3, time: 140.61610499999733 },
            { lane: 4, time: 141.03235499999937 },
            { lane: 3, time: 141.2158849999978 },
            { lane: 4, time: 141.41615999999703 },
            { lane: 5, time: 141.88320499999827 },
            { lane: 4, time: 142.31700999999885 },
            { lane: 3, time: 142.51716999999917 },
            { lane: 4, time: 142.71739499999967 },
            { lane: 3, time: 142.9509399999988 },
            { lane: 1, time: 143.16786499999944 },
            { lane: 2, time: 143.61827999999878 },
            { lane: 0, time: 144.0687399999988 },
            { lane: 1, time: 144.46926499999972 },
            { lane: 2, time: 144.8695099999968 },
            { lane: 3, time: 145.32009999999718 },
            { lane: 5, time: 145.70384999999806 },
            { lane: 4, time: 145.92055999999866 },
            { lane: 3, time: 146.1374450000003 },
            { lane: 2, time: 146.3543299999983 },
            { lane: 1, time: 146.5545349999993 },
            { lane: 2, time: 147.02166999999827 },
            { lane: 3, time: 147.4554649999991 },
            { lane: 4, time: 147.88918999999805 },
            { lane: 5, time: 148.33962999999858 },
            { lane: 4, time: 148.7566999999981 },
            { lane: 3, time: 149.1905149999984 },
            { lane: 4, time: 149.6243149999973 },
            { lane: 4, time: 150.0413950000002 },
            { lane: 4, time: 150.47507499999847 },
            { lane: 5, time: 150.8922050000001 },
            { lane: 5, time: 151.30927499999962 },
            { lane: 1, time: 151.75971500000014 },
            { lane: 1, time: 152.17684000000008 },
            { lane: 0, time: 152.59383499999967 },
            { lane: 0, time: 153.02759999999762 },
            { lane: 2, time: 153.47806499999933 },
            { lane: 2, time: 153.8784799999994 },
            { lane: 3, time: 154.31220499999836 },
            { lane: 1, time: 154.49572499999704 },
            { lane: 4, time: 154.7459799999997 },
            { lane: 5, time: 155.17980499999976 },
            { lane: 4, time: 155.6135549999999 },
            { lane: 3, time: 156.03065499999866 },
            { lane: 2, time: 156.4810449999968 },
            { lane: 1, time: 156.91485999999713 },
            { lane: 2, time: 157.29850499999884 },
            { lane: 5, time: 157.73227000000043 },
            { lane: 4, time: 158.14935999999943 },
            { lane: 3, time: 158.56644500000039 },
            { lane: 0, time: 159.05030999999872 },
            { lane: 1, time: 159.46733499999755 },
            { lane: 2, time: 159.91778499999782 },
            { lane: 3, time: 160.33487499999683 },
            { lane: 4, time: 160.76868999999715 },
            { lane: 5, time: 161.15241499999684 },
            { lane: 4, time: 161.58611499999824 },
            { lane: 3, time: 162.03654499999902 },
            { lane: 4, time: 162.4871099999982 },
            { lane: 5, time: 162.9041049999978 },
            { lane: 4, time: 163.33787999999913 },
            { lane: 3, time: 163.75492999999915 },
            { lane: 1, time: 164.20540499999697 },
            { lane: 2, time: 164.63915499999712 },
            { lane: 4, time: 165.0562549999995 },
            { lane: 5, time: 165.4566199999972 },
            { lane: 4, time: 165.90749999999753 },
            { lane: 3, time: 166.34103499999765 },
            { lane: 0, time: 166.79131499999858 },
            { lane: 1, time: 167.22509499999796 },
            { lane: 2, time: 167.6254749999971 },
            { lane: 0, time: 168.04253999999855 },
            { lane: 1, time: 168.50962499999878 },
            { lane: 2, time: 168.91005999999834 },
            { lane: 1, time: 169.32718499999828 },
            { lane: 2, time: 169.74420499999906 },
            { lane: 5, time: 170.17795999999726 },
            { lane: 4, time: 170.59527499999967 },
            { lane: 3, time: 171.04549000000043 },
            { lane: 5, time: 171.4625699999997 },
            { lane: 5, time: 171.91303999999946 },
            { lane: 5, time: 172.31345999999758 },
            { lane: 5, time: 172.74719499999992 },
            { lane: 4, time: 173.16437999999835 },
        ];
        this.activeBlocks = [];
    }

    getBlockSpawnYVelocity(speed = 1) {
        return speed * 96; // default speed is 96
    }

    getBlockSpawnYPosition(speed = 1) {
        return -((speed - 1) * 192); // default travel distance is 192
    }

    spawnBlock(lane, speed) {
        var that = this;
        if (lane >= 0 && lane <= 5) {
            var newBlock;
            switch (lane) {
                case 0:
                    newBlock = this.physics.add.image(68 + 14.5, this.getBlockSpawnYPosition(speed), "note_block_pink");
                    this.physics.add.overlap(newBlock, this.noteTrigger0, function () {
                        if (that.key0.isDown && newBlock.visible) {
                            newBlock.visible = false;
                            if (newBlock.y < 178) {
                                // console.log("missed"); // missed by early press
                                that.textStatus.setText("MISSED");
                                that.textStatus.visible = true;
                                that.countDownVisibleTimer = 1;
                                that.combo = 0;
                            } else {
                                if (newBlock.y >= 185 && newBlock.y <= 202) {
                                    // console.log("perfect"); // perfect hit
                                    that.score = that.score + (10 + 5 * (that.combo / 10));
                                    that.combo = that.combo + 1;

                                    that.textStatus.setText("PERFECT");
                                    that.textStatus.visible = true;
                                    that.countDownVisibleTimer = 1;
                                } else {
                                    // console.log("good"); // good hit
                                    that.score = that.score + (8 + 5 * (that.combo / 10));
                                    that.combo = that.combo + 1;

                                    that.textStatus.setText("GOOD");
                                    that.textStatus.visible = true;
                                    that.countDownVisibleTimer = 1;
                                }
                            }
                        }
                    });
                    break;
                case 1:
                    newBlock = this.physics.add.image(98 + 14.5, this.getBlockSpawnYPosition(speed), "note_block_blue");
                    this.physics.add.overlap(newBlock, this.noteTrigger1, function () {
                        if (that.key1.isDown && newBlock.visible) {
                            newBlock.visible = false;
                            if (newBlock.y < 178) {
                                that.textStatus.setText("MISSED");
                                that.textStatus.visible = true;
                                that.countDownVisibleTimer = 1;
                                that.combo = 0;
                            } else {
                                if (newBlock.y >= 185 && newBlock.y <= 202) {
                                    that.score = that.score + (10 + 5 * (that.combo / 10));
                                    that.combo = that.combo + 1;

                                    that.textStatus.setText("PERFECT");
                                    that.textStatus.visible = true;
                                    that.countDownVisibleTimer = 1;
                                } else {
                                    that.score = that.score + (8 + 5 * (that.combo / 10));
                                    that.combo = that.combo + 1;

                                    that.textStatus.setText("GOOD");
                                    that.textStatus.visible = true;
                                    that.countDownVisibleTimer = 1;
                                }
                            }
                        }
                    });
                    break;
                case 2:
                    newBlock = this.physics.add.image(128 + 14.5, this.getBlockSpawnYPosition(speed), "note_block_pink");
                    this.physics.add.overlap(newBlock, this.noteTrigger2, function () {
                        if (that.key2.isDown && newBlock.visible) {
                            newBlock.visible = false;
                            if (newBlock.y < 178) {
                                that.textStatus.setText("MISSED");
                                that.textStatus.visible = true;
                                that.countDownVisibleTimer = 1;
                                that.combo = 0;
                            } else {
                                if (newBlock.y >= 185 && newBlock.y <= 202) {
                                    that.score = that.score + (10 + 5 * (that.combo / 10));
                                    that.combo = that.combo + 1;

                                    that.textStatus.setText("PERFECT");
                                    that.textStatus.visible = true;
                                    that.countDownVisibleTimer = 1;
                                } else {
                                    that.score = that.score + (8 + 5 * (that.combo / 10));
                                    that.combo = that.combo + 1;

                                    that.textStatus.setText("GOOD");
                                    that.textStatus.visible = true;
                                    that.countDownVisibleTimer = 1;
                                }
                            }
                        }
                    });
                    break;
                case 3:
                    newBlock = this.physics.add.image(158 + 14.5, this.getBlockSpawnYPosition(speed), "note_block_pink");
                    this.physics.add.overlap(newBlock, this.noteTrigger3, function () {
                        if (that.key3.isDown && newBlock.visible) {
                            newBlock.visible = false;
                            if (newBlock.y < 178) {
                                that.textStatus.setText("MISSED");
                                that.textStatus.visible = true;
                                that.countDownVisibleTimer = 1;
                                that.combo = 0;
                            } else {
                                if (newBlock.y >= 185 && newBlock.y <= 202) {
                                    that.score = that.score + (10 + 5 * (that.combo / 10));
                                    that.combo = that.combo + 1;

                                    that.textStatus.setText("PERFECT");
                                    that.textStatus.visible = true;
                                    that.countDownVisibleTimer = 1;
                                } else {
                                    that.score = that.score + (8 + 5 * (that.combo / 10));
                                    that.combo = that.combo + 1;

                                    that.textStatus.setText("GOOD");
                                    that.textStatus.visible = true;
                                    that.countDownVisibleTimer = 1;
                                }
                            }
                        }
                    });
                    break;
                case 4:
                    newBlock = this.physics.add.image(188 + 14.5, this.getBlockSpawnYPosition(speed), "note_block_blue");
                    this.physics.add.overlap(newBlock, this.noteTrigger4, function () {
                        if (that.key4.isDown && newBlock.visible) {
                            newBlock.visible = false;
                            if (newBlock.y < 178) {
                                that.textStatus.setText("MISSED");
                                that.textStatus.visible = true;
                                that.countDownVisibleTimer = 1;
                                that.combo = 0;
                            } else {
                                if (newBlock.y >= 185 && newBlock.y <= 202) {
                                    that.score = that.score + (10 + 5 * (that.combo / 10));
                                    that.combo = that.combo + 1;

                                    that.textStatus.setText("PERFECT");
                                    that.textStatus.visible = true;
                                    that.countDownVisibleTimer = 1;
                                } else {
                                    that.score = that.score + (8 + 5 * (that.combo / 10));
                                    that.combo = that.combo + 1;

                                    that.textStatus.setText("GOOD");
                                    that.textStatus.visible = true;
                                    that.countDownVisibleTimer = 1;
                                }
                            }
                        }
                    });
                    break;
                case 5:
                    newBlock = this.physics.add.image(218 + 14.5, this.getBlockSpawnYPosition(speed), "note_block_pink");
                    this.physics.add.overlap(newBlock, this.noteTrigger5, function () {
                        if (that.key5.isDown && newBlock.visible) {
                            newBlock.visible = false;
                            if (newBlock.y < 178) {
                                that.textStatus.setText("MISSED");
                                that.textStatus.visible = true;
                                that.countDownVisibleTimer = 1;
                                that.combo = 0;
                            } else {
                                if (newBlock.y >= 185 && newBlock.y <= 202) {
                                    that.score = that.score + (10 + 5 * (that.combo / 10));
                                    that.combo = that.combo + 1;

                                    that.textStatus.setText("PERFECT");
                                    that.textStatus.visible = true;
                                    that.countDownVisibleTimer = 1;
                                } else {
                                    that.score = that.score + (8 + 5 * (that.combo / 10));
                                    that.combo = that.combo + 1;

                                    that.textStatus.setText("GOOD");
                                    that.textStatus.visible = true;
                                    that.countDownVisibleTimer = 1;
                                }
                            }
                        }
                    });
                    break;
            }
            newBlock.setOrigin(0, 0.5);
            newBlock.setVelocityY(this.getBlockSpawnYVelocity(speed));

            this.activeBlocks.push(newBlock);
        }
    }

    update(time, delta) {
        if (this.sceneStartTime == 0) {
            this.sceneStartTime = time;
        }
        this.sceneCurrentTime = time - this.sceneStartTime;
        if (this.sceneCurrentTime < 5000) {
            this.textStartCountdown.setText(5 - (this.sceneCurrentTime / 1000).toFixed(0));
        } else if (this.sceneCurrentTime >= 5000 && !this.song.isPlaying && this.songCurrentProgress == 0) {
            // song starts here
            this.textStartCountdown.visible = false;
            this.song.play();
            this.songStartTime = this.sceneCurrentTime;
            this.textScore.visible = true;
        } else if (this.song.isPlaying) {
            // song playing here
            this.songCurrentTime = (this.sceneCurrentTime - this.songStartTime) / 1000; // in second
            var remainingTime = this.song.duration + this.sceneCurrentTime / 1000 - (this.songCurrentTime + this.sceneCurrentTime / 1000); // in second
            this.songCurrentProgress = 1 - remainingTime / this.song.duration; // 0 - 1

            // spawn blocks here
            this.blockTimestamps.forEach((block) => {
                if (this.songCurrentTime >= block.time - 2.1) {
                    this.spawnBlock(block.lane, this.speed); // set spawn speed here
                    block.lane = -1;
                    block.time = this.song.duration * 255;
                }
            });

            // remove active blocks here
            for (var i = 0; i < this.activeBlocks.length; i++) {
                if (this.activeBlocks[i].y >= 203) {
                    if (this.activeBlocks[i].visible) {
                        // console.log("missed"); // missed by late press
                        this.combo = 0;

                        this.textStatus.setText("MISSED");
                        this.textStatus.visible = true;
                        this.countDownVisibleTimer = 1;
                    }
                    this.activeBlocks[i].destroy();
                    this.activeBlocks.splice(i, 1);
                }
            }

            this.textScore.setText(this.score.toFixed(0));

            if (this.countDownVisibleTimer > 0) {
                this.countDownVisibleTimer = this.countDownVisibleTimer - delta / 1000;
            } else {
                this.textStatus.visible = false;
            }
        } else {
            // song end
            this.textStatus.setText("Press MENU to Return");
            this.textStatus.y = 90;
            this.textStatus.visible = true;
        }

        if (this.key0.isDown) {
            this.spriteHighlightStrip0.visible = true;
        } else {
            this.spriteHighlightStrip0.visible = false;
        }

        if (this.key1.isDown) {
            this.spriteHighlightStrip1.visible = true;
        } else {
            this.spriteHighlightStrip1.visible = false;
        }

        if (this.key2.isDown) {
            this.spriteHighlightStrip2.visible = true;
        } else {
            this.spriteHighlightStrip2.visible = false;
        }

        if (this.key3.isDown) {
            this.spriteHighlightStrip3.visible = true;
        } else {
            this.spriteHighlightStrip3.visible = false;
        }

        if (this.key4.isDown) {
            this.spriteHighlightStrip4.visible = true;
        } else {
            this.spriteHighlightStrip4.visible = false;
        }

        if (this.key5.isDown) {
            this.spriteHighlightStrip5.visible = true;
        } else {
            this.spriteHighlightStrip5.visible = false;
        }
    }
}
