class Song1 extends Phaser.Scene {
    constructor() {
        super({ key: "Song1" });

        var sceneStartTime;
        var sceneCurrentTime;

        var songStartTime;
        var songCurrentTime;
        var songCurrentProgress;

        var song;

        var textStartCountdown;

        var spriteHighlightStrip0;
        var spriteHighlightStrip1;
        var spriteHighlightStrip2;
        var spriteHighlightStrip3;
        var spriteHighlightStrip4;
        var spriteHighlightStrip5;

        // input handler
        var key0;
        var key1;
        var key2;
        var key3;
        var key4;
        var key5;
        // input handler end

        var blockTimestamps;
        var activeBlocks; // current active blocks

        var noteTrigger0;
        var noteTrigger1;
        var noteTrigger2;
        var noteTrigger3;
        var noteTrigger4;
        var noteTrigger5;

        // score and combo
        var score;
        var textScore;
        var textStatus;
        var combo;
        var countDownVisibleTimer;

        var speed;
    }

    preload() {
        this.load.image("background_gameplay", "/game/assets/backgrounds/gameplay.png");
        this.load.audio("song", ["/game/assets/jake_kaufman/crypt_of_the_necrodancer_-_freestyle_retro/01_rhythmortis_(lobby_remix).ogg"]);

        this.load.image("kp_highlight_strip", "/game/assets/sprites/keypress_highlight_strip.png");

        this.load.image("note_block_pink", "/game/assets/sprites/note_block_pink.png");
        this.load.image("note_block_blue", "/game/assets/sprites/note_block_blue.png");

        this.load.image("note_trigger", "/game/assets/triggers/note_trigger.png");
    }

    create() {
        var background = this.add.image(0, 0, "background_gameplay");
        background.setOrigin(0, 0);

        this.input.keyboard.on(
            "keydown_ESC",
            function () {
                // location.href = "/game/index.html";
                this.song.stop();
                location.href = "/game/index.html";
                // this.scene.start("MainMenu");
            },
            this
        );

        this.sceneStartTime = 0;
        this.sceneCurrentTime = 0;

        this.songStartTime = 0;
        this.songCurrentTime = 0;
        this.songCurrentProgress = 0;

        this.song = this.sound.add("song");

        this.textStartCountdown = this.add.text(158, 40, "5", { font: "24px PressStart2P-Regular", align: "center" });
        this.textStartCountdown.setOrigin(0.5, 0.5);

        this.spriteHighlightStrip0 = this.add.image(68, 191, "kp_highlight_strip");
        this.spriteHighlightStrip0.setOrigin(0, 1);
        this.spriteHighlightStrip1 = this.add.image(98, 191, "kp_highlight_strip");
        this.spriteHighlightStrip1.setOrigin(0, 1);
        this.spriteHighlightStrip2 = this.add.image(128, 191, "kp_highlight_strip");
        this.spriteHighlightStrip2.setOrigin(0, 1);
        this.spriteHighlightStrip3 = this.add.image(158, 191, "kp_highlight_strip");
        this.spriteHighlightStrip3.setOrigin(0, 1);
        this.spriteHighlightStrip4 = this.add.image(188, 191, "kp_highlight_strip");
        this.spriteHighlightStrip4.setOrigin(0, 1);
        this.spriteHighlightStrip5 = this.add.image(218, 191, "kp_highlight_strip");
        this.spriteHighlightStrip5.setOrigin(0, 1);

        this.key0 = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.LEFT);
        this.key1 = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.UP);
        this.key2 = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.RIGHT);
        this.key3 = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.U);
        this.key4 = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.I);
        this.key5 = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.K);

        this.noteTrigger0 = this.physics.add.image(68, 198, "note_trigger");
        this.noteTrigger0.setOrigin(0, 1);
        this.noteTrigger0.setImmovable(true);
        this.noteTrigger1 = this.physics.add.image(98, 198, "note_trigger");
        this.noteTrigger1.setOrigin(0, 1);
        this.noteTrigger1.setImmovable(true);
        this.noteTrigger2 = this.physics.add.image(128, 198, "note_trigger");
        this.noteTrigger2.setOrigin(0, 1);
        this.noteTrigger2.setImmovable(true);
        this.noteTrigger3 = this.physics.add.image(158, 198, "note_trigger");
        this.noteTrigger3.setOrigin(0, 1);
        this.noteTrigger3.setImmovable(true);
        this.noteTrigger4 = this.physics.add.image(188, 198, "note_trigger");
        this.noteTrigger4.setOrigin(0, 1);
        this.noteTrigger4.setImmovable(true);
        this.noteTrigger5 = this.physics.add.image(218, 198, "note_trigger");
        this.noteTrigger5.setOrigin(0, 1);
        this.noteTrigger5.setImmovable(true);

        this.score = 0;
        this.textScore = this.add.text(158, 40, "0", { font: "16px PressStart2P-Regular", align: "center" });
        this.textScore.setOrigin(0.5, 0.5);
        this.textScore.visible = false;
        this.textStatus = this.add.text(158, 58, "PERFECT", { font: "12px PressStart2P-Regular", align: "center" });
        this.textStatus.setOrigin(0.5, 0.5);
        this.textStatus.visible = false;
        this.combo = 0;
        this.countDownVisibleTimer = 1;

        this.speed = 1.5;

        this.input.keyboard.on(
            "keydown_SPACE",
            function () {
                // slow down speed
                if (this.speed == 1.5) {
                    this.speed = 1;
                } else if (this.speed == 2) {
                    this.speed = 1.5;
                } else if (this.speed == 2.5) {
                    this.speed = 2;
                }
                // console.log(this.speed);
            },
            this
        );

        this.input.keyboard.on(
            "keydown_ENTER",
            function () {
                // increase speed
                if (this.speed == 1) {
                    this.speed = 1.5;
                } else if (this.speed == 1.5) {
                    this.speed = 2;
                } else if (this.speed == 2) {
                    this.speed = 2.5;
                }
                // console.log(this.speed);
            },
            this
        );

        // block spawn lanes and times
        // TODO: add notes to current song
        this.blockTimestamps = [
            { lane: 2, time: 7.464235000006738 },
            { lane: 3, time: 9.266025000004447 },
            { lane: 2, time: 11.102100000003702 },
            { lane: 3, time: 12.986355000000913 },
            { lane: 4, time: 14.137524999998277 },
            { lane: 5, time: 14.354449999998906 },
            { lane: 0, time: 14.804860000003828 },
            { lane: 0, time: 15.28880000000936 },
            { lane: 0, time: 15.755815000011353 },
            { lane: 0, time: 16.206279999998515 },
            { lane: 1, time: 16.656695000012405 },
            { lane: 1, time: 17.123830000011367 },
            { lane: 1, time: 17.591125000006286 },
            { lane: 1, time: 18.041400000001886 },
            { lane: 3, time: 18.508600000001024 },
            { lane: 3, time: 18.97566999999981 },
            { lane: 3, time: 19.44281000000774 },
            { lane: 3, time: 19.89336000000185 },
            { lane: 4, time: 20.377095000003465 },
            { lane: 4, time: 20.827530000009574 },
            { lane: 4, time: 21.294705000007525 },
            { lane: 4, time: 21.761810000010883 },
            { lane: 5, time: 22.228940000000875 },
            { lane: 5, time: 22.66273500000534 },
            { lane: 5, time: 23.14646499999799 },
            { lane: 5, time: 23.596940000003087 },
            { lane: 2, time: 24.064155000000028 },
            { lane: 2, time: 24.514795000010054 },
            { lane: 2, time: 24.964984999998705 },
            { lane: 2, time: 25.432215000008 },
            { lane: 3, time: 25.882525000008172 },
            { lane: 3, time: 26.416830000001937 },
            { lane: 4, time: 26.800115000005462 },
            { lane: 4, time: 27.283940000008442 },
            { lane: 2, time: 27.75108500001079 },
            { lane: 2, time: 28.201520000002347 },
            { lane: 0, time: 28.651960000002873 },
            { lane: 0, time: 29.119130000006407 },
            { lane: 2, time: 29.58624000000418 },
            { lane: 2, time: 30.737410000001546 },
            { lane: 3, time: 30.95429000000877 },
            { lane: 4, time: 31.171275000000605 },
            { lane: 5, time: 31.421410000009928 },
            { lane: 4, time: 33.28990500001237 },
            { lane: 3, time: 34.19093500000599 },
            { lane: 2, time: 35.1084150000097 },
            { lane: 1, time: 36.04412000000593 },
            { lane: 0, time: 36.976940000007744 },
            { lane: 0, time: 37.427410000003874 },
            { lane: 0, time: 37.87802499999816 },
            { lane: 3, time: 38.36163500000839 },
            { lane: 0, time: 38.84552500001155 },
            { lane: 0, time: 39.279235000009066 },
            { lane: 0, time: 39.729670000000624 },
            { lane: 0, time: 40.196985000002314 },
            { lane: 3, time: 40.69729000001098 },
            { lane: 4, time: 41.61498500000744 },
            { lane: 5, time: 42.51576500000374 },
            { lane: 2, time: 43.416635000001406 },
            { lane: 2, time: 44.334425000008196 },
            { lane: 0, time: 46.21951999999874 },
            { lane: 3, time: 48.05501000001095 },
            { lane: 3, time: 48.371615000010934 },
            { lane: 3, time: 48.70524999999907 },
            { lane: 3, time: 49.906455000003916 },
            { lane: 3, time: 50.22343500000716 },
            { lane: 3, time: 50.5407300000079 },
            { lane: 2, time: 51.74169999999867 },
            { lane: 3, time: 53.59347000000707 },
            { lane: 3, time: 55.445275000005495 },
            { lane: 3, time: 55.7623600000079 },
            { lane: 3, time: 56.0960699999996 },
            { lane: 3, time: 57.280435000007856 },
            { lane: 3, time: 57.6142000000109 },
            { lane: 4, time: 57.94781500000681 },
            { lane: 5, time: 59.13231000000087 },
            { lane: 4, time: 60.950815000003786 },
            { lane: 1, time: 62.80262000000221 },
            { lane: 0, time: 64.67115999999805 },
        ];
        this.activeBlocks = [];
    }

    getBlockSpawnYVelocity(speed = 1) {
        return speed * 96; // default speed is 96
    }

    getBlockSpawnYPosition(speed = 1) {
        return -((speed - 1) * 192); // default travel distance is 192
    }

    spawnBlock(lane, speed) {
        var that = this;
        if (lane >= 0 && lane <= 5) {
            var newBlock;
            switch (lane) {
                case 0:
                    newBlock = this.physics.add.image(68 + 14.5, this.getBlockSpawnYPosition(speed), "note_block_pink");
                    this.physics.add.overlap(newBlock, this.noteTrigger0, function () {
                        if (that.key0.isDown && newBlock.visible) {
                            newBlock.visible = false;
                            if (newBlock.y < 178) {
                                // console.log("missed"); // missed by early press
                                that.textStatus.setText("MISSED");
                                that.textStatus.visible = true;
                                that.countDownVisibleTimer = 1;
                                that.combo = 0;
                            } else {
                                if (newBlock.y >= 185 && newBlock.y <= 202) {
                                    // console.log("perfect"); // perfect hit
                                    that.score = that.score + (10 + 5 * (that.combo / 10));
                                    that.combo = that.combo + 1;

                                    that.textStatus.setText("PERFECT");
                                    that.textStatus.visible = true;
                                    that.countDownVisibleTimer = 1;
                                } else {
                                    // console.log("good"); // good hit
                                    that.score = that.score + (8 + 5 * (that.combo / 10));
                                    that.combo = that.combo + 1;

                                    that.textStatus.setText("GOOD");
                                    that.textStatus.visible = true;
                                    that.countDownVisibleTimer = 1;
                                }
                            }
                        }
                    });
                    break;
                case 1:
                    newBlock = this.physics.add.image(98 + 14.5, this.getBlockSpawnYPosition(speed), "note_block_blue");
                    this.physics.add.overlap(newBlock, this.noteTrigger1, function () {
                        if (that.key1.isDown && newBlock.visible) {
                            newBlock.visible = false;
                            if (newBlock.y < 178) {
                                that.textStatus.setText("MISSED");
                                that.textStatus.visible = true;
                                that.countDownVisibleTimer = 1;
                                that.combo = 0;
                            } else {
                                if (newBlock.y >= 185 && newBlock.y <= 202) {
                                    that.score = that.score + (10 + 5 * (that.combo / 10));
                                    that.combo = that.combo + 1;

                                    that.textStatus.setText("PERFECT");
                                    that.textStatus.visible = true;
                                    that.countDownVisibleTimer = 1;
                                } else {
                                    that.score = that.score + (8 + 5 * (that.combo / 10));
                                    that.combo = that.combo + 1;

                                    that.textStatus.setText("GOOD");
                                    that.textStatus.visible = true;
                                    that.countDownVisibleTimer = 1;
                                }
                            }
                        }
                    });
                    break;
                case 2:
                    newBlock = this.physics.add.image(128 + 14.5, this.getBlockSpawnYPosition(speed), "note_block_pink");
                    this.physics.add.overlap(newBlock, this.noteTrigger2, function () {
                        if (that.key2.isDown && newBlock.visible) {
                            newBlock.visible = false;
                            if (newBlock.y < 178) {
                                that.textStatus.setText("MISSED");
                                that.textStatus.visible = true;
                                that.countDownVisibleTimer = 1;
                                that.combo = 0;
                            } else {
                                if (newBlock.y >= 185 && newBlock.y <= 202) {
                                    that.score = that.score + (10 + 5 * (that.combo / 10));
                                    that.combo = that.combo + 1;

                                    that.textStatus.setText("PERFECT");
                                    that.textStatus.visible = true;
                                    that.countDownVisibleTimer = 1;
                                } else {
                                    that.score = that.score + (8 + 5 * (that.combo / 10));
                                    that.combo = that.combo + 1;

                                    that.textStatus.setText("GOOD");
                                    that.textStatus.visible = true;
                                    that.countDownVisibleTimer = 1;
                                }
                            }
                        }
                    });
                    break;
                case 3:
                    newBlock = this.physics.add.image(158 + 14.5, this.getBlockSpawnYPosition(speed), "note_block_pink");
                    this.physics.add.overlap(newBlock, this.noteTrigger3, function () {
                        if (that.key3.isDown && newBlock.visible) {
                            newBlock.visible = false;
                            if (newBlock.y < 178) {
                                that.textStatus.setText("MISSED");
                                that.textStatus.visible = true;
                                that.countDownVisibleTimer = 1;
                                that.combo = 0;
                            } else {
                                if (newBlock.y >= 185 && newBlock.y <= 202) {
                                    that.score = that.score + (10 + 5 * (that.combo / 10));
                                    that.combo = that.combo + 1;

                                    that.textStatus.setText("PERFECT");
                                    that.textStatus.visible = true;
                                    that.countDownVisibleTimer = 1;
                                } else {
                                    that.score = that.score + (8 + 5 * (that.combo / 10));
                                    that.combo = that.combo + 1;

                                    that.textStatus.setText("GOOD");
                                    that.textStatus.visible = true;
                                    that.countDownVisibleTimer = 1;
                                }
                            }
                        }
                    });
                    break;
                case 4:
                    newBlock = this.physics.add.image(188 + 14.5, this.getBlockSpawnYPosition(speed), "note_block_blue");
                    this.physics.add.overlap(newBlock, this.noteTrigger4, function () {
                        if (that.key4.isDown && newBlock.visible) {
                            newBlock.visible = false;
                            if (newBlock.y < 178) {
                                that.textStatus.setText("MISSED");
                                that.textStatus.visible = true;
                                that.countDownVisibleTimer = 1;
                                that.combo = 0;
                            } else {
                                if (newBlock.y >= 185 && newBlock.y <= 202) {
                                    that.score = that.score + (10 + 5 * (that.combo / 10));
                                    that.combo = that.combo + 1;

                                    that.textStatus.setText("PERFECT");
                                    that.textStatus.visible = true;
                                    that.countDownVisibleTimer = 1;
                                } else {
                                    that.score = that.score + (8 + 5 * (that.combo / 10));
                                    that.combo = that.combo + 1;

                                    that.textStatus.setText("GOOD");
                                    that.textStatus.visible = true;
                                    that.countDownVisibleTimer = 1;
                                }
                            }
                        }
                    });
                    break;
                case 5:
                    newBlock = this.physics.add.image(218 + 14.5, this.getBlockSpawnYPosition(speed), "note_block_pink");
                    this.physics.add.overlap(newBlock, this.noteTrigger5, function () {
                        if (that.key5.isDown && newBlock.visible) {
                            newBlock.visible = false;
                            if (newBlock.y < 178) {
                                that.textStatus.setText("MISSED");
                                that.textStatus.visible = true;
                                that.countDownVisibleTimer = 1;
                                that.combo = 0;
                            } else {
                                if (newBlock.y >= 185 && newBlock.y <= 202) {
                                    that.score = that.score + (10 + 5 * (that.combo / 10));
                                    that.combo = that.combo + 1;

                                    that.textStatus.setText("PERFECT");
                                    that.textStatus.visible = true;
                                    that.countDownVisibleTimer = 1;
                                } else {
                                    that.score = that.score + (8 + 5 * (that.combo / 10));
                                    that.combo = that.combo + 1;

                                    that.textStatus.setText("GOOD");
                                    that.textStatus.visible = true;
                                    that.countDownVisibleTimer = 1;
                                }
                            }
                        }
                    });
                    break;
            }
            newBlock.setOrigin(0, 0.5);
            newBlock.setVelocityY(this.getBlockSpawnYVelocity(speed));

            this.activeBlocks.push(newBlock);
        }
    }

    update(time, delta) {
        if (this.sceneStartTime == 0) {
            this.sceneStartTime = time;
        }
        this.sceneCurrentTime = time - this.sceneStartTime;
        if (this.sceneCurrentTime < 5000) {
            this.textStartCountdown.setText(5 - (this.sceneCurrentTime / 1000).toFixed(0));
        } else if (this.sceneCurrentTime >= 5000 && !this.song.isPlaying && this.songCurrentProgress == 0) {
            // song starts here
            this.textStartCountdown.visible = false;
            this.song.play();
            this.songStartTime = this.sceneCurrentTime;
            this.textScore.visible = true;
        } else if (this.song.isPlaying) {
            // song playing here
            this.songCurrentTime = (this.sceneCurrentTime - this.songStartTime) / 1000; // in second
            var remainingTime = this.song.duration + this.sceneCurrentTime / 1000 - (this.songCurrentTime + this.sceneCurrentTime / 1000); // in second
            this.songCurrentProgress = 1 - remainingTime / this.song.duration; // 0 - 1

            // spawn blocks here
            this.blockTimestamps.forEach((block) => {
                if (this.songCurrentTime >= block.time - 2.1) {
                    this.spawnBlock(block.lane, this.speed); // set spawn speed here
                    block.lane = -1;
                    block.time = this.song.duration * 255;
                }
            });

            // remove active blocks here
            for (var i = 0; i < this.activeBlocks.length; i++) {
                if (this.activeBlocks[i].y >= 203) {
                    if (this.activeBlocks[i].visible) {
                        // console.log("missed"); // missed by late press
                        this.combo = 0;

                        this.textStatus.setText("MISSED");
                        this.textStatus.visible = true;
                        this.countDownVisibleTimer = 1;
                    }
                    this.activeBlocks[i].destroy();
                    this.activeBlocks.splice(i, 1);
                }
            }

            this.textScore.setText(this.score.toFixed(0));

            if (this.countDownVisibleTimer > 0) {
                this.countDownVisibleTimer = this.countDownVisibleTimer - delta / 1000;
            } else {
                this.textStatus.visible = false;
            }
        } else {
            // song end
            this.textStatus.setText("Press MENU to Return");
            this.textStatus.y = 90;
            this.textStatus.visible = true;
        }

        if (this.key0.isDown) {
            this.spriteHighlightStrip0.visible = true;
        } else {
            this.spriteHighlightStrip0.visible = false;
        }

        if (this.key1.isDown) {
            this.spriteHighlightStrip1.visible = true;
        } else {
            this.spriteHighlightStrip1.visible = false;
        }

        if (this.key2.isDown) {
            this.spriteHighlightStrip2.visible = true;
        } else {
            this.spriteHighlightStrip2.visible = false;
        }

        if (this.key3.isDown) {
            this.spriteHighlightStrip3.visible = true;
        } else {
            this.spriteHighlightStrip3.visible = false;
        }

        if (this.key4.isDown) {
            this.spriteHighlightStrip4.visible = true;
        } else {
            this.spriteHighlightStrip4.visible = false;
        }

        if (this.key5.isDown) {
            this.spriteHighlightStrip5.visible = true;
        } else {
            this.spriteHighlightStrip5.visible = false;
        }
    }
}
