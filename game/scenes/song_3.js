class Song3 extends Phaser.Scene {
    constructor() {
        super({ key: "Song3" });

        var sceneStartTime;
        var sceneCurrentTime;

        var songStartTime;
        var songCurrentTime;
        var songCurrentProgress;

        var song;

        var textStartCountdown;

        var spriteHighlightStrip0;
        var spriteHighlightStrip1;
        var spriteHighlightStrip2;
        var spriteHighlightStrip3;
        var spriteHighlightStrip4;
        var spriteHighlightStrip5;

        // input handler
        var key0;
        var key1;
        var key2;
        var key3;
        var key4;
        var key5;
        // input handler end

        var blockTimestamps;
        var activeBlocks; // current active blocks

        var noteTrigger0;
        var noteTrigger1;
        var noteTrigger2;
        var noteTrigger3;
        var noteTrigger4;
        var noteTrigger5;

        // score and combo
        var score;
        var textScore;
        var textStatus;
        var combo;
        var countDownVisibleTimer;

        var speed;
    }

    preload() {
        this.load.image("background_gameplay", "/game/assets/backgrounds/gameplay.png");
        this.load.audio("song", ["/game/assets/jake_kaufman/crypt_of_the_necrodancer_-_freestyle_retro/03_crypteque_(1-2_remix).ogg"]);

        this.load.image("kp_highlight_strip", "/game/assets/sprites/keypress_highlight_strip.png");

        this.load.image("note_block_pink", "/game/assets/sprites/note_block_pink.png");
        this.load.image("note_block_blue", "/game/assets/sprites/note_block_blue.png");

        this.load.image("note_trigger", "/game/assets/triggers/note_trigger.png");
    }

    create() {
        var background = this.add.image(0, 0, "background_gameplay");
        background.setOrigin(0, 0);

        this.input.keyboard.on(
            "keydown_ESC",
            function () {
                // location.href = "/game/index.html";
                this.song.stop();
                location.href = "/game/index.html";
                // this.scene.start("MainMenu");
            },
            this
        );

        this.sceneStartTime = 0;
        this.sceneCurrentTime = 0;

        this.songStartTime = 0;
        this.songCurrentTime = 0;
        this.songCurrentProgress = 0;

        this.song = this.sound.add("song");

        this.textStartCountdown = this.add.text(158, 40, "5", { font: "24px PressStart2P-Regular", align: "center" });
        this.textStartCountdown.setOrigin(0.5, 0.5);

        this.spriteHighlightStrip0 = this.add.image(68, 191, "kp_highlight_strip");
        this.spriteHighlightStrip0.setOrigin(0, 1);
        this.spriteHighlightStrip1 = this.add.image(98, 191, "kp_highlight_strip");
        this.spriteHighlightStrip1.setOrigin(0, 1);
        this.spriteHighlightStrip2 = this.add.image(128, 191, "kp_highlight_strip");
        this.spriteHighlightStrip2.setOrigin(0, 1);
        this.spriteHighlightStrip3 = this.add.image(158, 191, "kp_highlight_strip");
        this.spriteHighlightStrip3.setOrigin(0, 1);
        this.spriteHighlightStrip4 = this.add.image(188, 191, "kp_highlight_strip");
        this.spriteHighlightStrip4.setOrigin(0, 1);
        this.spriteHighlightStrip5 = this.add.image(218, 191, "kp_highlight_strip");
        this.spriteHighlightStrip5.setOrigin(0, 1);

        this.key0 = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.LEFT);
        this.key1 = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.UP);
        this.key2 = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.RIGHT);
        this.key3 = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.U);
        this.key4 = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.I);
        this.key5 = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.K);

        this.noteTrigger0 = this.physics.add.image(68, 198, "note_trigger");
        this.noteTrigger0.setOrigin(0, 1);
        this.noteTrigger0.setImmovable(true);
        this.noteTrigger1 = this.physics.add.image(98, 198, "note_trigger");
        this.noteTrigger1.setOrigin(0, 1);
        this.noteTrigger1.setImmovable(true);
        this.noteTrigger2 = this.physics.add.image(128, 198, "note_trigger");
        this.noteTrigger2.setOrigin(0, 1);
        this.noteTrigger2.setImmovable(true);
        this.noteTrigger3 = this.physics.add.image(158, 198, "note_trigger");
        this.noteTrigger3.setOrigin(0, 1);
        this.noteTrigger3.setImmovable(true);
        this.noteTrigger4 = this.physics.add.image(188, 198, "note_trigger");
        this.noteTrigger4.setOrigin(0, 1);
        this.noteTrigger4.setImmovable(true);
        this.noteTrigger5 = this.physics.add.image(218, 198, "note_trigger");
        this.noteTrigger5.setOrigin(0, 1);
        this.noteTrigger5.setImmovable(true);

        this.score = 0;
        this.textScore = this.add.text(158, 40, "0", { font: "16px PressStart2P-Regular", align: "center" });
        this.textScore.setOrigin(0.5, 0.5);
        this.textScore.visible = false;
        this.textStatus = this.add.text(158, 58, "PERFECT", { font: "12px PressStart2P-Regular", align: "center" });
        this.textStatus.setOrigin(0.5, 0.5);
        this.textStatus.visible = false;
        this.combo = 0;
        this.countDownVisibleTimer = 1;

        this.speed = 1.5;

        this.input.keyboard.on(
            "keydown_SPACE",
            function () {
                // slow down speed
                if (this.speed == 1.5) {
                    this.speed = 1;
                } else if (this.speed == 2) {
                    this.speed = 1.5;
                } else if (this.speed == 2.5) {
                    this.speed = 2;
                }
                // console.log(this.speed);
            },
            this
        );

        this.input.keyboard.on(
            "keydown_ENTER",
            function () {
                // increase speed
                if (this.speed == 1) {
                    this.speed = 1.5;
                } else if (this.speed == 1.5) {
                    this.speed = 2;
                } else if (this.speed == 2) {
                    this.speed = 2.5;
                }
                // console.log(this.speed);
            },
            this
        );

        // block spawn lanes and times
        // TODO: add notes to current song
        this.blockTimestamps = [
            { lane: 2, time: 3.749210000001767 },
            { lane: 2, time: 3.749210000001767 },
            { lane: 3, time: 3.749210000001767 },
            { lane: 3, time: 4.66682000000219 },
            { lane: 2, time: 4.685120000001916 },
            { lane: 3, time: 5.63437999999951 },
            { lane: 3, time: 5.63437999999951 },
            { lane: 2, time: 5.63437999999951 },
            { lane: 1, time: 6.5519899999999325 },
            { lane: 1, time: 6.5519899999999325 },
            { lane: 4, time: 6.5519899999999325 },
            { lane: 0, time: 6.985740000000078 },
            { lane: 0, time: 6.985740000000078 },
            { lane: 5, time: 6.985740000000078 },
            { lane: 4, time: 7.469629999999597 },
            { lane: 4, time: 7.936734999999317 },
            { lane: 4, time: 8.403819999999541 },
            { lane: 4, time: 8.854240000000573 },
            { lane: 4, time: 9.290254999999888 },
            { lane: 4, time: 9.72188499999902 },
            { lane: 3, time: 10.188975000000937 },
            { lane: 5, time: 10.622709999999643 },
            { lane: 4, time: 11.139914999999746 },
            { lane: 4, time: 11.590319999999338 },
            { lane: 4, time: 12.024085000000923 },
            { lane: 4, time: 12.491249999999127 },
            { lane: 3, time: 12.975024999999732 },
            { lane: 3, time: 13.442275000001246 },
            { lane: 2, time: 13.90957999999955 },
            { lane: 1, time: 14.3263850000003 },
            { lane: 0, time: 14.810209999999643 },
            { lane: 1, time: 15.277385000001232 },
            { lane: 1, time: 15.744455000000016 },
            { lane: 1, time: 16.211569999999483 },
            { lane: 2, time: 16.64542000000074 },
            { lane: 4, time: 17.112475000001723 },
            { lane: 1, time: 17.57970000000205 },
            { lane: 2, time: 18.013405000001512 },
            { lane: 3, time: 18.497159999998985 },
            { lane: 3, time: 18.964404999998806 },
            { lane: 4, time: 19.4314200000008 },
            { lane: 4, time: 19.881884999998874 },
            { lane: 5, time: 20.349075000001903 },
            { lane: 4, time: 20.766084999999293 },
            { lane: 3, time: 21.283265000001848 },
            { lane: 2, time: 21.70035999999891 },
            { lane: 1, time: 22.150789999999688 },
            { lane: 0, time: 22.617930000000342 },
            { lane: 1, time: 22.968284999999014 },
            { lane: 2, time: 23.10182999999961 },
            { lane: 1, time: 23.568865000001097 },
            { lane: 2, time: 24.002759999999398 },
            { lane: 4, time: 24.469850000001315 },
            { lane: 5, time: 24.90406499999881 },
            { lane: 4, time: 25.387510000000475 },
            { lane: 3, time: 25.888090000000375 },
            { lane: 3, time: 26.038015000001906 },
            { lane: 3, time: 26.188154999999824 },
            { lane: 3, time: 26.33835000000181 },
            { lane: 2, time: 26.822140000000218 },
            { lane: 1, time: 27.27257499999905 },
            { lane: 2, time: 27.773055000001477 },
            { lane: 3, time: 28.223500000000058 },
            { lane: 4, time: 28.707415000000765 },
            { lane: 5, time: 29.124410000000353 },
            { lane: 0, time: 29.574875000002066 },
            { lane: 1, time: 30.042010000001028 },
            { lane: 2, time: 30.492450000001554 },
            { lane: 2, time: 30.992935000002035 },
            { lane: 1, time: 31.476730000002135 },
            { lane: 2, time: 31.910530000001017 },
            { lane: 3, time: 32.27756000000227 },
            { lane: 4, time: 32.39429000000018 },
            { lane: 5, time: 32.81142000000182 },
            { lane: 4, time: 33.295225000001665 },
            { lane: 3, time: 33.76240500000131 },
            { lane: 3, time: 34.22949000000153 },
            { lane: 3, time: 34.67989999999918 },
            { lane: 3, time: 35.163759999999456 },
            { lane: 0, time: 35.5975099999996 },
            { lane: 0, time: 35.5975099999996 },
            { lane: 3, time: 35.5975099999996 },
            { lane: 1, time: 36.081334999998944 },
            { lane: 2, time: 36.382314999998925 },
            { lane: 4, time: 36.481705000001966 },
            { lane: 5, time: 36.848744999999326 },
            { lane: 4, time: 36.99884999999995 },
            { lane: 4, time: 37.48270500000217 },
            { lane: 4, time: 37.9832000000024 },
            { lane: 4, time: 38.350280000002385 },
            { lane: 3, time: 38.83413500000097 },
            { lane: 2, time: 39.16768500000035 },
            { lane: 1, time: 39.26781000000119 },
            { lane: 2, time: 39.75161500000104 },
            { lane: 4, time: 40.23549500000081 },
            { lane: 0, time: 40.66928000000189 },
            { lane: 5, time: 41.136890000001586 },
            { lane: 1, time: 41.620145000000775 },
            { lane: 4, time: 42.07067499999903 },
            { lane: 2, time: 42.537765000000945 },
            { lane: 3, time: 43.00493500000084 },
            { lane: 1, time: 43.45531000000119 },
            { lane: 4, time: 43.92246499999965 },
            { lane: 5, time: 44.339514999999665 },
            { lane: 5, time: 44.8233450000007 },
            { lane: 5, time: 45.290469999999914 },
            { lane: 5, time: 45.757580000001326 },
            { lane: 4, time: 46.224764999999024 },
            { lane: 4, time: 46.6919999999991 },
            { lane: 3, time: 47.125630000002275 },
            { lane: 2, time: 47.576075000000856 },
            { lane: 5, time: 48.043219999999565 },
            { lane: 0, time: 48.05986000000121 },
            { lane: 1, time: 48.4769750000014 },
            { lane: 2, time: 48.994174999999814 },
            { lane: 1, time: 49.46132000000216 },
            { lane: 2, time: 49.91176500000074 },
            { lane: 1, time: 50.34557000000132 },
            { lane: 4, time: 50.81264999999985 },
            { lane: 5, time: 51.26304499999969 },
            { lane: 0, time: 51.71349000000191 },
            { lane: 0, time: 52.19739499999923 },
            { lane: 0, time: 52.6811200000011 },
            { lane: 0, time: 53.114925000001676 },
            { lane: 1, time: 53.58220000000074 },
            { lane: 2, time: 54.015784999999596 },
            { lane: 3, time: 54.499579999999696 },
            { lane: 4, time: 54.95011499999964 },
            { lane: 5, time: 55.40055000000211 },
            { lane: 5, time: 55.884375000001455 },
            { lane: 4, time: 56.31805499999973 },
            { lane: 3, time: 56.78523000000132 },
            { lane: 2, time: 57.235725000002276 },
            { lane: 1, time: 57.71944000000076 },
            { lane: 2, time: 58.16991500000222 },
            { lane: 3, time: 58.60367000000042 },
            { lane: 4, time: 59.08787500000108 },
            { lane: 5, time: 59.554605000001175 },
            { lane: 4, time: 60.03844500000196 },
            { lane: 3, time: 60.4723699999995 },
            { lane: 2, time: 60.93942500000048 },
            { lane: 1, time: 61.43994999999995 },
            { lane: 0, time: 61.906989999999496 },
            { lane: 1, time: 62.3574950000002 },
            { lane: 2, time: 62.80801500000234 },
            { lane: 2, time: 63.29174000000057 },
            { lane: 2, time: 63.7253950000013 },
            { lane: 2, time: 64.19258500000069 },
            { lane: 3, time: 64.64298500000223 },
            { lane: 4, time: 65.12683499999912 },
            { lane: 5, time: 65.5772500000021 },
            { lane: 5, time: 66.02773499999967 },
            { lane: 4, time: 66.51155999999901 },
            { lane: 5, time: 66.94527500000186 },
            { lane: 4, time: 67.46256500000163 },
            { lane: 3, time: 67.89622500000041 },
            { lane: 4, time: 68.21323499999926 },
            { lane: 4, time: 68.3634349999993 },
            { lane: 5, time: 68.83097500000076 },
            { lane: 4, time: 69.29768999999942 },
            { lane: 5, time: 69.73150000000169 },
            { lane: 4, time: 70.23196000000098 },
            { lane: 5, time: 70.66572499999893 },
            { lane: 3, time: 71.1328050000011 },
            { lane: 3, time: 71.59999500000049 },
            { lane: 4, time: 71.91690500000186 },
            { lane: 2, time: 72.10051999999996 },
            { lane: 2, time: 72.51750000000175 },
            { lane: 1, time: 72.96794500000033 },
            { lane: 1, time: 73.4351999999999 },
            { lane: 0, time: 73.90226500000063 },
            { lane: 0, time: 74.35263499999928 },
            { lane: 1, time: 74.81983000000037 },
            { lane: 2, time: 75.2369949999993 },
            { lane: 2, time: 75.60388000000239 },
            { lane: 1, time: 75.73739000000205 },
            { lane: 2, time: 76.20449500000177 },
            { lane: 2, time: 76.68828000000212 },
            { lane: 2, time: 77.12214999999924 },
            { lane: 2, time: 77.58922499999971 },
            { lane: 2, time: 78.05634999999893 },
            { lane: 3, time: 78.50676000000021 },
            { lane: 1, time: 78.95726500000092 },
            { lane: 5, time: 79.42440500000157 },
            { lane: 5, time: 79.42440500000157 },
            { lane: 0, time: 79.42440500000157 },
            { lane: 0, time: 79.87483500000235 },
            { lane: 0, time: 80.35861999999906 },
            { lane: 1, time: 80.82573000000048 },
            { lane: 2, time: 81.10934500000076 },
            { lane: 3, time: 81.27617499999906 },
            { lane: 3, time: 81.7599850000006 },
            { lane: 4, time: 82.21042500000112 },
            { lane: 5, time: 82.67756999999983 },
            { lane: 5, time: 83.11135500000091 },
            { lane: 0, time: 83.12819500000114 },
            { lane: 0, time: 83.57845000000088 },
            { lane: 0, time: 84.07950500000152 },
            { lane: 0, time: 84.52947000000131 },
            { lane: 1, time: 84.84648500000185 },
            { lane: 1, time: 85.07997500000056 },
            { lane: 1, time: 85.48039000000063 },
            { lane: 2, time: 85.74732000000222 },
            { lane: 2, time: 85.98090500000035 },
            { lane: 2, time: 86.24777499999982 },
            { lane: 2, time: 86.53139999999985 },
            { lane: 1, time: 86.7984550000001 },
            { lane: 2, time: 87.24881000000096 },
            { lane: 3, time: 87.66610000000219 },
            { lane: 4, time: 88.1666750000004 },
            { lane: 5, time: 88.60017000000153 },
            { lane: 4, time: 89.08398999999918 },
            { lane: 3, time: 89.53449499999988 },
            { lane: 2, time: 90.00154499999917 },
            { lane: 1, time: 90.50207000000228 },
            { lane: 2, time: 90.9526700000024 },
            { lane: 4, time: 91.38645000000179 },
            { lane: 4, time: 91.8700749999989 },
            { lane: 4, time: 92.30385500000193 },
            { lane: 4, time: 92.77114000000074 },
            { lane: 3, time: 93.22141499999998 },
            { lane: 2, time: 93.70525500000076 },
            { lane: 0, time: 94.17237500000192 },
            { lane: 0, time: 94.63949499999944 },
            { lane: 0, time: 95.08994000000166 },
            { lane: 0, time: 95.52368500000011 },
            { lane: 1, time: 95.9741150000009 },
            { lane: 1, time: 96.44150000000081 },
            { lane: 2, time: 96.92508000000089 },
            { lane: 2, time: 97.40890500000023 },
            { lane: 5, time: 97.85940000000119 },
            { lane: 5, time: 98.32649500000116 },
            { lane: 4, time: 98.77688000000126 },
            { lane: 4, time: 99.27736999999979 },
            { lane: 5, time: 99.71116500000062 },
            { lane: 5, time: 100.19507499999963 },
            { lane: 3, time: 100.66219999999885 },
            { lane: 3, time: 101.11252500000046 },
            { lane: 2, time: 101.56311999999889 },
            { lane: 4, time: 102.03014500000063 },
            { lane: 1, time: 102.4805950000009 },
            { lane: 4, time: 102.94772500000181 },
            { lane: 5, time: 103.41483000000153 },
            { lane: 5, time: 103.41483000000153 },
            { lane: 0, time: 103.41483000000153 },
            { lane: 1, time: 103.8653149999991 },
            { lane: 2, time: 104.3490750000019 },
            { lane: 4, time: 104.79952500000218 },
            { lane: 3, time: 105.2500199999995 },
            { lane: 1, time: 105.70048999999926 },
            { lane: 4, time: 106.18427499999962 },
            { lane: 5, time: 106.63473000000158 },
            { lane: 2, time: 107.08514999999898 },
            { lane: 3, time: 107.55225500000233 },
            { lane: 4, time: 108.00277999999889 },
            { lane: 5, time: 108.4865400000017 },
            { lane: 1, time: 108.9370400000007 },
            { lane: 2, time: 109.38751000000047 },
            { lane: 3, time: 109.85465000000113 },
            { lane: 0, time: 110.3383849999991 },
            { lane: 1, time: 110.80550000000221 },
            { lane: 4, time: 111.27263999999923 },
            { lane: 5, time: 111.73977000000014 },
            { lane: 4, time: 112.22360000000117 },
            { lane: 3, time: 112.65736499999912 },
            { lane: 2, time: 113.10781000000134 },
            { lane: 1, time: 113.59166499999992 },
            { lane: 2, time: 114.02541000000201 },
            { lane: 4, time: 114.44245000000228 },
            { lane: 5, time: 114.92628500000137 },
            { lane: 3, time: 115.42678499999965 },
            { lane: 4, time: 115.86060999999972 },
            { lane: 1, time: 116.3276650000007 },
            { lane: 2, time: 116.77809999999954 },
            { lane: 0, time: 117.26197999999931 },
            { lane: 2, time: 117.71236000000135 },
            { lane: 1, time: 118.19620000000214 },
            { lane: 2, time: 118.66340499999933 },
            { lane: 1, time: 119.130465000002 },
            { lane: 2, time: 119.5808849999994 },
            { lane: 4, time: 120.06471000000238 },
            { lane: 3, time: 120.51523499999894 },
            { lane: 2, time: 120.94893500000035 },
            { lane: 1, time: 121.43278500000088 },
            { lane: 2, time: 121.89990000000034 },
            { lane: 2, time: 122.4003300000004 },
            { lane: 2, time: 122.86753999999928 },
            { lane: 2, time: 123.35128500000064 },
            { lane: 3, time: 123.73505500000101 },
            { lane: 3, time: 124.652610000001 },
            { lane: 2, time: 125.58683500000188 },
            { lane: 4, time: 126.50442000000112 },
            { lane: 5, time: 126.95498000000225 },
            { lane: 2, time: 127.40533499999947 },
            { lane: 2, time: 128.3562950000014 },
            { lane: 2, time: 129.30731500000184 },
            { lane: 3, time: 130.1747400000022 },
            { lane: 4, time: 130.62525000000096 },
            { lane: 1, time: 131.0924100000011 },
            { lane: 1, time: 132.02657499999987 },
            { lane: 1, time: 132.94422000000122 },
            { lane: 4, time: 133.8950900000018 },
            { lane: 5, time: 134.34555499999988 },
            { lane: 1, time: 134.81267500000104 },
            { lane: 0, time: 135.26310000000012 },
            { lane: 1, time: 135.7469349999992 },
            { lane: 0, time: 136.19738499999949 },
            { lane: 1, time: 136.69790999999896 },
            { lane: 2, time: 137.1316850000003 },
            { lane: 3, time: 137.61550999999963 },
            { lane: 4, time: 138.06593000000066 },
            { lane: 5, time: 138.53303500000038 },
            { lane: 4, time: 138.96682000000146 },
            { lane: 3, time: 139.43395500000042 },
            { lane: 2, time: 139.88439500000095 },
            { lane: 5, time: 140.36826999999903 },
            { lane: 4, time: 140.81869500000175 },
            { lane: 3, time: 141.26914500000203 },
            { lane: 2, time: 141.7028449999998 },
            { lane: 4, time: 142.18675000000076 },
            { lane: 3, time: 142.65381999999954 },
            { lane: 2, time: 143.10428500000125 },
            { lane: 1, time: 143.55468499999915 },
            { lane: 4, time: 144.03858000000037 },
            { lane: 3, time: 144.47231000000102 },
            { lane: 2, time: 144.93942500000048 },
            { lane: 1, time: 145.3899100000017 },
            { lane: 3, time: 145.87366499999916 },
            { lane: 2, time: 146.32413499999893 },
            { lane: 1, time: 146.80795500000022 },
            { lane: 0, time: 147.2417150000001 },
            { lane: 3, time: 147.74222500000178 },
            { lane: 2, time: 148.17596500000218 },
            { lane: 1, time: 148.64310000000114 },
            { lane: 0, time: 149.07690999999977 },
            { lane: 5, time: 149.57734999999957 },
            { lane: 4, time: 150.06115000000136 },
            { lane: 3, time: 150.51167500000156 },
            { lane: 2, time: 150.97874500000034 },
            { lane: 3, time: 151.44588999999905 },
            { lane: 2, time: 151.91300000000047 },
            { lane: 1, time: 152.34679999999935 },
            { lane: 0, time: 152.76387500000055 },
            { lane: 3, time: 153.2643449999996 },
            { lane: 2, time: 153.2812350000022 },
            { lane: 4, time: 153.7315000000017 },
            { lane: 1, time: 154.19858000000022 },
            { lane: 5, time: 154.66572000000087 },
            { lane: 0, time: 155.11621500000183 },
            { lane: 4, time: 155.58330000000205 },
            { lane: 1, time: 156.0671450000009 },
            { lane: 3, time: 156.51756500000192 },
            { lane: 2, time: 156.9680100000005 },
            { lane: 2, time: 156.9680100000005 },
            { lane: 3, time: 156.9680100000005 },
            { lane: 4, time: 157.4518500000013 },
            { lane: 5, time: 157.88661000000138 },
            { lane: 1, time: 158.36945000000196 },
            { lane: 0, time: 158.78653499999928 },
            { lane: 2, time: 159.25373000000036 },
            { lane: 1, time: 159.72085000000152 },
            { lane: 2, time: 160.1545499999993 },
            { lane: 4, time: 160.6384399999988 },
            { lane: 5, time: 161.10550499999954 },
        ];
        this.activeBlocks = [];
    }

    getBlockSpawnYVelocity(speed = 1) {
        return speed * 96; // default speed is 96
    }

    getBlockSpawnYPosition(speed = 1) {
        return -((speed - 1) * 192); // default travel distance is 192
    }

    spawnBlock(lane, speed) {
        var that = this;
        if (lane >= 0 && lane <= 5) {
            var newBlock;
            switch (lane) {
                case 0:
                    newBlock = this.physics.add.image(68 + 14.5, this.getBlockSpawnYPosition(speed), "note_block_pink");
                    this.physics.add.overlap(newBlock, this.noteTrigger0, function () {
                        if (that.key0.isDown && newBlock.visible) {
                            newBlock.visible = false;
                            if (newBlock.y < 178) {
                                // console.log("missed"); // missed by early press
                                that.textStatus.setText("MISSED");
                                that.textStatus.visible = true;
                                that.countDownVisibleTimer = 1;
                                that.combo = 0;
                            } else {
                                if (newBlock.y >= 185 && newBlock.y <= 202) {
                                    // console.log("perfect"); // perfect hit
                                    that.score = that.score + (10 + 5 * (that.combo / 10));
                                    that.combo = that.combo + 1;

                                    that.textStatus.setText("PERFECT");
                                    that.textStatus.visible = true;
                                    that.countDownVisibleTimer = 1;
                                } else {
                                    // console.log("good"); // good hit
                                    that.score = that.score + (8 + 5 * (that.combo / 10));
                                    that.combo = that.combo + 1;

                                    that.textStatus.setText("GOOD");
                                    that.textStatus.visible = true;
                                    that.countDownVisibleTimer = 1;
                                }
                            }
                        }
                    });
                    break;
                case 1:
                    newBlock = this.physics.add.image(98 + 14.5, this.getBlockSpawnYPosition(speed), "note_block_blue");
                    this.physics.add.overlap(newBlock, this.noteTrigger1, function () {
                        if (that.key1.isDown && newBlock.visible) {
                            newBlock.visible = false;
                            if (newBlock.y < 178) {
                                that.textStatus.setText("MISSED");
                                that.textStatus.visible = true;
                                that.countDownVisibleTimer = 1;
                                that.combo = 0;
                            } else {
                                if (newBlock.y >= 185 && newBlock.y <= 202) {
                                    that.score = that.score + (10 + 5 * (that.combo / 10));
                                    that.combo = that.combo + 1;

                                    that.textStatus.setText("PERFECT");
                                    that.textStatus.visible = true;
                                    that.countDownVisibleTimer = 1;
                                } else {
                                    that.score = that.score + (8 + 5 * (that.combo / 10));
                                    that.combo = that.combo + 1;

                                    that.textStatus.setText("GOOD");
                                    that.textStatus.visible = true;
                                    that.countDownVisibleTimer = 1;
                                }
                            }
                        }
                    });
                    break;
                case 2:
                    newBlock = this.physics.add.image(128 + 14.5, this.getBlockSpawnYPosition(speed), "note_block_pink");
                    this.physics.add.overlap(newBlock, this.noteTrigger2, function () {
                        if (that.key2.isDown && newBlock.visible) {
                            newBlock.visible = false;
                            if (newBlock.y < 178) {
                                that.textStatus.setText("MISSED");
                                that.textStatus.visible = true;
                                that.countDownVisibleTimer = 1;
                                that.combo = 0;
                            } else {
                                if (newBlock.y >= 185 && newBlock.y <= 202) {
                                    that.score = that.score + (10 + 5 * (that.combo / 10));
                                    that.combo = that.combo + 1;

                                    that.textStatus.setText("PERFECT");
                                    that.textStatus.visible = true;
                                    that.countDownVisibleTimer = 1;
                                } else {
                                    that.score = that.score + (8 + 5 * (that.combo / 10));
                                    that.combo = that.combo + 1;

                                    that.textStatus.setText("GOOD");
                                    that.textStatus.visible = true;
                                    that.countDownVisibleTimer = 1;
                                }
                            }
                        }
                    });
                    break;
                case 3:
                    newBlock = this.physics.add.image(158 + 14.5, this.getBlockSpawnYPosition(speed), "note_block_pink");
                    this.physics.add.overlap(newBlock, this.noteTrigger3, function () {
                        if (that.key3.isDown && newBlock.visible) {
                            newBlock.visible = false;
                            if (newBlock.y < 178) {
                                that.textStatus.setText("MISSED");
                                that.textStatus.visible = true;
                                that.countDownVisibleTimer = 1;
                                that.combo = 0;
                            } else {
                                if (newBlock.y >= 185 && newBlock.y <= 202) {
                                    that.score = that.score + (10 + 5 * (that.combo / 10));
                                    that.combo = that.combo + 1;

                                    that.textStatus.setText("PERFECT");
                                    that.textStatus.visible = true;
                                    that.countDownVisibleTimer = 1;
                                } else {
                                    that.score = that.score + (8 + 5 * (that.combo / 10));
                                    that.combo = that.combo + 1;

                                    that.textStatus.setText("GOOD");
                                    that.textStatus.visible = true;
                                    that.countDownVisibleTimer = 1;
                                }
                            }
                        }
                    });
                    break;
                case 4:
                    newBlock = this.physics.add.image(188 + 14.5, this.getBlockSpawnYPosition(speed), "note_block_blue");
                    this.physics.add.overlap(newBlock, this.noteTrigger4, function () {
                        if (that.key4.isDown && newBlock.visible) {
                            newBlock.visible = false;
                            if (newBlock.y < 178) {
                                that.textStatus.setText("MISSED");
                                that.textStatus.visible = true;
                                that.countDownVisibleTimer = 1;
                                that.combo = 0;
                            } else {
                                if (newBlock.y >= 185 && newBlock.y <= 202) {
                                    that.score = that.score + (10 + 5 * (that.combo / 10));
                                    that.combo = that.combo + 1;

                                    that.textStatus.setText("PERFECT");
                                    that.textStatus.visible = true;
                                    that.countDownVisibleTimer = 1;
                                } else {
                                    that.score = that.score + (8 + 5 * (that.combo / 10));
                                    that.combo = that.combo + 1;

                                    that.textStatus.setText("GOOD");
                                    that.textStatus.visible = true;
                                    that.countDownVisibleTimer = 1;
                                }
                            }
                        }
                    });
                    break;
                case 5:
                    newBlock = this.physics.add.image(218 + 14.5, this.getBlockSpawnYPosition(speed), "note_block_pink");
                    this.physics.add.overlap(newBlock, this.noteTrigger5, function () {
                        if (that.key5.isDown && newBlock.visible) {
                            newBlock.visible = false;
                            if (newBlock.y < 178) {
                                that.textStatus.setText("MISSED");
                                that.textStatus.visible = true;
                                that.countDownVisibleTimer = 1;
                                that.combo = 0;
                            } else {
                                if (newBlock.y >= 185 && newBlock.y <= 202) {
                                    that.score = that.score + (10 + 5 * (that.combo / 10));
                                    that.combo = that.combo + 1;

                                    that.textStatus.setText("PERFECT");
                                    that.textStatus.visible = true;
                                    that.countDownVisibleTimer = 1;
                                } else {
                                    that.score = that.score + (8 + 5 * (that.combo / 10));
                                    that.combo = that.combo + 1;

                                    that.textStatus.setText("GOOD");
                                    that.textStatus.visible = true;
                                    that.countDownVisibleTimer = 1;
                                }
                            }
                        }
                    });
                    break;
            }
            newBlock.setOrigin(0, 0.5);
            newBlock.setVelocityY(this.getBlockSpawnYVelocity(speed));

            this.activeBlocks.push(newBlock);
        }
    }

    update(time, delta) {
        if (this.sceneStartTime == 0) {
            this.sceneStartTime = time;
        }
        this.sceneCurrentTime = time - this.sceneStartTime;
        if (this.sceneCurrentTime < 5000) {
            this.textStartCountdown.setText(5 - (this.sceneCurrentTime / 1000).toFixed(0));
        } else if (this.sceneCurrentTime >= 5000 && !this.song.isPlaying && this.songCurrentProgress == 0) {
            // song starts here
            this.textStartCountdown.visible = false;
            this.song.play();
            this.songStartTime = this.sceneCurrentTime;
            this.textScore.visible = true;
        } else if (this.song.isPlaying) {
            // song playing here
            this.songCurrentTime = (this.sceneCurrentTime - this.songStartTime) / 1000; // in second
            var remainingTime = this.song.duration + this.sceneCurrentTime / 1000 - (this.songCurrentTime + this.sceneCurrentTime / 1000); // in second
            this.songCurrentProgress = 1 - remainingTime / this.song.duration; // 0 - 1

            // spawn blocks here
            this.blockTimestamps.forEach((block) => {
                if (this.songCurrentTime >= block.time - 2.1) {
                    this.spawnBlock(block.lane, this.speed); // set spawn speed here
                    block.lane = -1;
                    block.time = this.song.duration * 255;
                }
            });

            // remove active blocks here
            for (var i = 0; i < this.activeBlocks.length; i++) {
                if (this.activeBlocks[i].y >= 203) {
                    if (this.activeBlocks[i].visible) {
                        // console.log("missed"); // missed by late press
                        this.combo = 0;

                        this.textStatus.setText("MISSED");
                        this.textStatus.visible = true;
                        this.countDownVisibleTimer = 1;
                    }
                    this.activeBlocks[i].destroy();
                    this.activeBlocks.splice(i, 1);
                }
            }

            this.textScore.setText(this.score.toFixed(0));

            if (this.countDownVisibleTimer > 0) {
                this.countDownVisibleTimer = this.countDownVisibleTimer - delta / 1000;
            } else {
                this.textStatus.visible = false;
            }
        } else {
            // song end
            this.textStatus.setText("Press MENU to Return");
            this.textStatus.y = 90;
            this.textStatus.visible = true;
        }

        if (this.key0.isDown) {
            this.spriteHighlightStrip0.visible = true;
        } else {
            this.spriteHighlightStrip0.visible = false;
        }

        if (this.key1.isDown) {
            this.spriteHighlightStrip1.visible = true;
        } else {
            this.spriteHighlightStrip1.visible = false;
        }

        if (this.key2.isDown) {
            this.spriteHighlightStrip2.visible = true;
        } else {
            this.spriteHighlightStrip2.visible = false;
        }

        if (this.key3.isDown) {
            this.spriteHighlightStrip3.visible = true;
        } else {
            this.spriteHighlightStrip3.visible = false;
        }

        if (this.key4.isDown) {
            this.spriteHighlightStrip4.visible = true;
        } else {
            this.spriteHighlightStrip4.visible = false;
        }

        if (this.key5.isDown) {
            this.spriteHighlightStrip5.visible = true;
        } else {
            this.spriteHighlightStrip5.visible = false;
        }
    }
}
