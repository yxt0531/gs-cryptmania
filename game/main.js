var config = {
    type: Phaser.AUTO,
    width: 316,
    height: 236,
    physics: {
        default: "arcade",
        arcade: {
            fps: 60,
        },
    },
    scene: [MainMenu, Song1, Song2, Song3, Song4, Song5],
};

var game = new Phaser.Game(config);
