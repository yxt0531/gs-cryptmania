# License

[Crypt of the NecroDancer - Freestyle Retro](https://virt.bandcamp.com/album/crypt-of-the-necrodancer-freestyle-retro)

[Attribution-NonCommercial 3.0 Unported (CC BY-NC 3.0)](https://creativecommons.org/licenses/by-nc/3.0/)

-Original music by Danny Baranowsky
-Arrangements by Jake Kaufman
-Crypt of the NecroDancer is owned/developed by Brace Yourselves Games
-Logistical and moral support by Jayson Napolitano / Scarlet Moon Productions
